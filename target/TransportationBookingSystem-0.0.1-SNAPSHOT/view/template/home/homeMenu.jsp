<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <style>
  .modal-header, h4, .close {
      background-color: #5cb85c;
      color:white !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-footer {
      background-color: red;
  }
  </style>
  <style>
  body {
     /*  position: relative;  */
  }
 #section1 {padding-top:50px;height:500px;color:black; background-color: white;}
  #section2 {padding-top:50px;height:500px;color:black; background-color: white;}
  #section3 {padding-top:50px;height:500px;color:black; background-color:white;}
  #section41 {padding-top:50px;height:500px;color:black; background-color: white;}
  #section42 {padding-top:50px;height:500px;color:black; background-color: white;}
  </style> 

       <!--Home Login Validation  Link-->
<script type="text/javascript" src="js/homeloginValidation.js"></script>




</head>
<body>


  <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar  navbar">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Transportation Booking System</a>
    </div>
   
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
         <li><a href="home.tiles">HOME</a></li>
          <li><a href="about.tiles">ABOUT US</a></li>
          <li><a href="serviceHome.tiles">SERVICE</a></li>
          <li><a href="contactHome.tiles">CONTACT US</a></li>
          <li><a href="locationHome.tiles">IN-MAP_LOCATION</a></li>
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">BRANCH<span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#section41">PUNE</a></li>
              <li><a href="#section42">MUMBAI</a></li>
            </ul>
          </li>
          </ul>
        <ul class="nav navbar-nav navbar-right">
     <!--   <li><a href="#"><span class="glyphicon glyphicon-user" style="color: white"></span> Sign Up</a></li> -->
        
        
        <li><span class="glyphicon glyphicon-user" style="color: white"><button style="background-color: #222;
     border-color: #080808;margin-top: 14px" data-toggle="modal" data-target="#myModal2">SingUp</button></span>
        
        
        
   <!--  <li><a href="login.tiles" ><span class="glyphicon glyphicon-log-in"></span> Log In</a></li>  -->
   
        <li><span class="glyphicon glyphicon-log-in" style="color: white"><button style="background-color: #222;
     border-color: #080808;margin-top: 14px" data-toggle="modal" data-target="#myModal">Login</button></span>
        
        
        </li>
        </ul>
         
       </div>  
     </div>
   </nav> 
   

   <!--Login Popup window  -->
   
   <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button"  class="close" data-dismiss="modal">&times;</button>
          <h4><span class="glyphicon glyphicon-lock" ></span> Login</h4>
        </div>
        
        
        <!--Form Action login  -->
       
        
 
        
        <form action="login" method="post" >
        <div class="modal-body">
         <div class="form-group">
              <label for="usrname"><span class="glyphicon glyphicon-user"></span> Username</label>
              <input type="text" class="form-control" id="usrname" name="usrname" placeholder="Enter email">
            </div>
   
         <div class="form-group">
              <label for="password"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Enter password">
            </div>
       
       
         <div class="checkbox">
      <label><input type="checkbox" name="remember"> Remember me</label>
    </div>
     <button type="submit" class="btn btn-success btn-block" id="Homelogin" ><span class="glyphicon glyphicon-off"></span> Login</button>
     </div>
 </form>
      </div>  
    </div>
  </div>
  
                         <!--Sign Up Section  -->
                         
                         <script type="text/javascript">
                         
                             
                         
                         $(document).ready(function (){
                        	 
                        	
                        	    
                        	    $("#HomeRegistration").click(function (){
                        	    	var firstname = $("#firstname").val();
                        	    	var lastname = $("#lastname").val();
                        	    	var mobileNO = $("#mno").val();
                        	    	var Address = $("#add").val();
                        	    	var email = $("#mail").val();
                        	    	var password = $("#psw").val();
                        	    	var cpassword = $("#repsw").val();

                        	    	   if (firstname == ''|| lastname == '' || mobileNO == '' ||Address == '' ||email == '' || password == '' || cpassword == '') 
                        	    	     {	
                        	    	     alert("Please fill all fields...!!!!!!");
                        	    	     return false;
                        	    	     } 
                        	    	  
                        	    	else if ((password.length) < 8)  
                        	    	      {
                        	    		alert("Password should atleast 8 character in length...!!!!!!");
                        	    		return false;
                        	    	      } 
                        	    	else if (!(password).match(cpassword))
                        	    	        {

                        	    		alert("Your passwords don't match. Try again?");
                        	    		return false;
                        	    	         } 
                        	    	
                        	    	});
                        	    });
                         
                         
                         
                         </script>
                         
                         
                         
                         
                         <div class="container">
 
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" style="padding:35px 50px;">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4><span  class="glyphicon glyphicon-lock"></span>Register</h4>
        </div>
        
        <div class="modal-body" style="padding:40px 50px;">
        
          <form   id="reg" role="form"  action="homeRegistration" method="post">
        
            <div class="form-group">
              <label for="firstname"><span class="glyphicon glyphicon-user"></span>Firstname</label>
              <input type="text" class="form-control" id="firstname" placeholder="Enter firstName" name="Firstname">
            </div>
        
        <div class="form-group">
              <label for="Lastname"><span class="glyphicon glyphicon-user"></span>Lastname</label>
              <input type="text" class="form-control" id="lastname" placeholder="Enter lastName" name="Lastname">
            
            </div>
        
        
        
            <div class="form-group">
              <label for="psw"><span class="glyphicon glyphicon-eye-open"></span> Password</label>
              <input type="password" class="form-control" id="psw" placeholder="Enter password" name="Password">
            </div>
        
        <div class="form-group">
              <label for="Repsw"><span class="glyphicon glyphicon-eye-open"></span> Re-Password</label>
              <input type="password" class="form-control" id="repsw" placeholder="Enter confirmpassword" name=" Re-Password">
            </div>
        
        
        <div class="form-group">
              <label for="mobileNO"><span class="glyphicon glyphicon-phone"></span>MobileNo</label>
              <input type="text" class="form-control" id="mno" placeholder="Enter phoneNo" name="MobileNo">
            </div>
        
        	<div class="form-group">
              <label for="Address"><span class="glyphicon glyphicon-home"></span>Address</label>
              <input type="text" class="form-control" id="add" placeholder="Enter address" name="Address">
            </div>
        
        	<div class="form-group">
              <label for="mail"><span class="glyphicon glyphicon-envelope"></span>Email</label>
              <input type="email" class="form-control" id="mail" placeholder="Enter Email" name="Email">
            </div>
       
            	<div class="form-group">
            	<input type="hidden" class="form-control" value="Customer" name="UserType">
            	</div>
       
            
     <div class="checkbox">
              <label><input type="checkbox" value="" checked>Remember me</label>
            </div>
        
              <button type="submit" class="btn btn-success btn-block" id="HomeRegistration"><span class="glyphicon glyphicon-off" ></span> Login</button>
          </form>
        </div>
        
        
      </div>
    </div>
  </div> 
</div>
                         
                         
                         
                         
                         
                         

<!-- <div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    Indicators
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    Wrapper for slides
    <div class="carousel-inner">

      <div class="item active">
        <img src="img/1.jpg" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
          <h3>By Road</h3>
          <p>All India Permite</p>
        </div>
      </div>

      <div class="item">
        <img src="img/home2.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <h3>By Shipe</h3>
          <p>We Are Responsive To Diliver Goods</p>
        </div>
      </div>
    
      <div class="item">
        <img src="img/home3.jpg" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <h3>Storge</h3>
          <p>Safely Store</p>
        </div>
      </div>
  
    </div>

    Left and right controls
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div> -->

</body>
</html>