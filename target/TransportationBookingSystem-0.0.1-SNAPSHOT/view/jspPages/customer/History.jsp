<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Properties"%>
<%@page import="java.sql.Connection"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>History Customer</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<%  String username=(String)session.getAttribute("username");
 Connection con=(Connection)session.getAttribute("Connection");
  						
 	Properties properties=(Properties)session.getAttribute("properties");
	String sql=properties.getProperty("showing_history_customer");
	PreparedStatement pst=con.prepareStatement(sql);
	pst.setString(1, username);
	
	ResultSet rs=pst.executeQuery();
	
 	while(rs.next())
 	{
%>
<div class="container" style= "width:100%;  height:80%; ">

  <h2>Customer History</h2>
  <p></p>
  <table class="table">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Mobile_No</th>
        <th>booking_count</th>
        <th>Address</th>
      </tr>
      </thead>
<tbody>
<tr class="success">
<td><%=rs.getString(1) %></td>
<td><%=rs.getString(2) %></td>
<td><%=rs.getString(3) %></td>
<td><%=rs.getString(4) %></td>
<%-- <td><%=rs.getString(5) %></td> --%>
</tbody>

</table>
</div>

<%} %>
</body>
</html>