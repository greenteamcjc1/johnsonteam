<!DOCTYPE html>
<html lang="en">
<head>
<style type="text/css">


div.stars {

  width: 270px;

  display: inline-block;

}

input.star { display: none; }


label.star {

  float: right;

  padding: 10px;

  font-size: 36px;

  color: #444;

  transition: all .2s;

}

input.star:checked ~ label.star:before {

  content: '\f005';

  color: #FD4;


}

input.star-5:checked ~ label.star:before {

  color: #FE7;

  text-shadow: 0 0 20px #952;

}

input.star-1:checked ~ label.star:before { color: #F62; }

label.star:hover { transform: rotate(-15deg) scale(1.3); }

label.star:before {

  content: '\f006';

  font-family: FontAwesome;

}



</style>
<meta charset="utf-8">
<title>FeedBack</title>
<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="http://webthemez.com" /> -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
</head>
<body>
<form action="feedback">
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Feedback</h2>
			</div>
		</div>
	</div>
	</section>
	<section id="content">
	
	<div class="container">
		<div class="row"> 
							<div class="col-md-12">
								<div class="about-logo">
									<h3>Get In touch with us<span class="color">...Transportation</span></h3>
									<p> Your Feedback is valuable to us please give us feedback us for providung excellent service in future</p>
								</div>  
							</div>
						</div>
	<div class="row">
								<div class="col-md-6">
									<p> </p>
								  	
		   <!-- Form itself -->
          <form action="feedback" id="contactForm"  novalidate> 
		 <div class="input-field">
		   <label for="name" class="">   Name </label>  
			<input type="text" name="names" class="form-control" 
			   	   id="name" required
			           data-validation-required-message="Please enter your name" />
					 
			  <p class="help-block"></p>
		   
	         </div> 	
                <div class="input-field"> 
                  <label for="name" class="">Email</label> 
			<input type="email" class="form-control" id="email" name="emails"required
			   		   data-validation-required-message="Please enter your email" /> 
					 
	    </div> 	
	    
	  <label for="name" class="">mobileNo</label>
			<input type="text" name="mobileno" class="form-control" 
			   	   id="name" required
			           data-validation-required-message="Please enter your name" />
					 
			  <p class="help-block"></p>
	    
			  
               <div class="input-field">
                <label for="name" class="">Feedback </label> 
             
				 <textarea rows="10" cols="100" name="feedback" required class="form-control materialize-textarea" 
                       idation-required-message="Please enter your message" minlength="5" 
                       data-validation-minlength-message="Min 5 characters" 
                        maxlength="999" style="resize:none"></textarea>
						 
		  </div> 		 
	     <div id="success"> </div> For success/fail messages
	     <br>
	     <div class="stars">

  

    <input class="star star-5" id="star-5" type="radio" name="star" value="5"/>

    <label class="star star-5" for="star-5"></label>

    <input class="star star-4" id="star-4" type="radio" name="star" value="4"/>

    <label class="star star-4" for="star-4"></label>

    <input class="star star-3" id="star-3" type="radio" name="star" value="3"/>

    <label class="star star-3" for="star-3"></label>

    <input class="star star-2" id="star-2" type="radio" name="star" value="2"/>

    <label class="star star-2" for="star-2"></label>

    <input class="star star-1" id="star-1" type="radio" name="star" value="1"/>

    <label class="star star-1" for="star-1"></label>


   <input type="submit" class="btn btn-primary waves-effect waves-dark pull-right" name="Send" style="margin-left: 0px" value="Send"><br />


	   
          </form>
</div>          
          
							</div>
	</div>
 
	</section>

<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="materialize/js/materialize.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.fancybox.pack.js"></script>
<script src="js/jquery.fancybox-media.js"></script>  
<script src="js/jquery.flexslider.js"></script>
<script src="js/animate.js"></script>
Vendor Scripts
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/animate.js"></script> 
<script src="js/custom.js"></script>

 <script src="contact/jqBootstrapValidation.js"></script>
 <script src="contact/contact_me.js"></script> -->
</body>
</html>