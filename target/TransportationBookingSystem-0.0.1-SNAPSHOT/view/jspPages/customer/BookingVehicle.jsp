<!DOCTYPE html>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Properties"%>
<html class=''>
<head>
<script
	src='//production-assets.codepen.io/assets/editor/live/console_runner-079c09a0e3b9ff743e39ee2d5637b9216b3545af0de366d4b9aad9dc87e26bfd.js'></script>
<script
	src='//production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>
<script
	src='//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>
<meta charset='UTF-8'>
<meta name="robots" content="noindex">
<link rel="shortcut icon" type="image/x-icon"
	href="//production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
<link rel="mask-icon" type=""
	href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg"
	color="#111" />
<link rel="canonical" href="https://codepen.io/anon/pen/vJjZQB" />

<link rel='stylesheet prefetch'
	href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'>
<style class="cp-pen-styles">/*custom font*/
@import url(https://fonts.googleapis.com/css?family=Montserrat);

/*basic reset*/
* {
	margin: 0;
	padding: 0;
}

html {
	height: 100%;
	/*Image only BG fallback*/
	/*background = gradient + image pattern combo*/
	background-image: url(3d-purple-flower-field-wallpaper.jpg);

	/* background: 
		linear-gradient(rgba(196, 102, 0, 0.6), rgba(155, 89, 182, 0.6)); */
}

body {
	font-family: montserrat, arial, verdana;
}
/*form styles*/
#msform {
	width: 400px;
	margin: 50px auto;
	text-align: center;
	position: relative;
}

#msform fieldset {
	background: white;
	border: 0 none;
	border-radius: 3px;
	box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
	padding: 20px 30px;
	box-sizing: border-box;
	width: 80%;
	margin: 0 10%;
	/*stacking fieldsets above each other*/
	position: relative;
}
/*Hide all except first fieldset*/
#msform fieldset:not (:first-of-type ) {
	display: none;
}
/*inputs*/
#msform input, #msform textarea {
	padding: 15px;
	border: 1px solid #ccc;
	border-radius: 3px;
	margin-bottom: 10px;
	width: 100%;
	box-sizing: border-box;
	font-family: montserrat;
	color: #2C3E50;
	font-size: 13px;
}
/*buttons*/
#msform .action-button {
	width: 100px;
	background: #27AE60;
	font-weight: bold;
	color: white;
	border: 0 none;
	border-radius: 1px;
	cursor: pointer;
	padding: 10px 5px;
	margin: 10px 5px;
}

#msform .action-button:hover, #msform .action-button:focus {
	box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
}
/*headings*/
.fs-title {
	font-size: 15px;
	text-transform: uppercase;
	color: #2C3E50;
	margin-bottom: 10px;
}

.fs-subtitle {
	font-weight: normal;
	font-size: 13px;
	color: #666;
	margin-bottom: 20px;
}
/*progressbar*/
#progressbar {
	margin-bottom: 30px;
	overflow: hidden;
	/*CSS counters to number the steps*/
	counter-reset: step;
}

#progressbar li {
	list-style-type: none;
	color: white;
	text-transform: uppercase;
	font-size: 9px;
	width: 33.33%;
	float: left;
	position: relative;
}

#progressbar li:before {
	content: counter(step);
	counter-increment: step;
	width: 20px;
	line-height: 20px;
	display: block;
	font-size: 10px;
	color: #333;
	background: white;
	border-radius: 3px;
	margin: 0 auto 5px auto;
}
/*progressbar connectors*/
#progressbar li:after {
	content: '';
	width: 100%;
	height: 2px;
	background: white;
	position: absolute;
	left: -50%;
	top: 9px;
	z-index: -1; /*put it behind the numbers*/
}

#progressbar li:first-child:after {
	/*connector not needed before the first step*/
	content: none;
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
#progressbar li.active:before, #progressbar li.active:after {
	background: #27AE60;
	color: white;
}
</style>
</head>	
<body>
	<!-- multistep form -->

<% String email= (String)session.getAttribute("username");
  
       out.print(email);
				Connection connection=(Connection)session.getAttribute("Connection");			

			Properties properties=(Properties)session.getAttribute("properties");
								
			String sql=properties.getProperty("login_getcustomerdetails");
			
			PreparedStatement preparedStatement=connection.prepareStatement(sql);
				preparedStatement.setString(1, email);
				ResultSet rs=preparedStatement.executeQuery();
	
				while(rs.next())
				{
%>


	

	<form  action="bookingAction" name="fname" id="msform">
		<!-- progressbar -->
		<ul id="progressbar">
			<li class="active">Personal Details</li>
			<li>Time And Date</li>
			<li>Info</li>
			<li>payment</li>
		</ul>

						
		<b><fieldset>
			<h2 class="fs-title">Personal Details</h2>
				<h3 class="fs-subtitle">This is step 1</h3>

			
			<input type="text" name="fname" placeholder="First Name" value=<%=rs.getString(1)%>> <input
				type="text" name="lname" placeholder="Last Name" value=<%=rs.getString(2) %> > <input
				type="text" name="phone" placeholder="Phone" value=<%=rs.getString(3) %>>
			<textarea name="address" placeholder="Address" value=<%=rs.getString(4) %>></textarea>
			<input type="text" name="email" placeholder="Email" value=<%=rs.getString(5) %> > <input
				type="button" name="next" class="next action-button" value="Next" >
		</fieldset></b>

		<fieldset>
			<h2 class="fs-title">Time And Date</h2>
			<h3 class="fs-subtitle">second step</h3>
			
			<input type="Date" name="Date" placeholder="Date" placeholder="Date" />
			<input type="Time" name="Time" placeholder="Time" placeholder="hh-mm" />

			<input type="text" name="placeFrom" placeholder="City From ex.pune" />
			<input type="text" name="placeTO" placeholder="City TO ex.mumbai" />

			<input type="button" name="previous" class="previous action-button"
				value="Previous" /> <input type="button" name="next"
				class="next action-button" value="Next" />
		</fieldset>

		<fieldset>
			<h2 class="fs-title">Info</h2>
			<h3 class="fs-subtitle">Details</h3>
		<input type="hidden" name="vehicleno" value="Empty">
			<input type="text" name="weight" placeholder="weight" /> <input
				type="text" name="vehicle" placeholder="No.of vehicle" />
			<textarea name="deliveredaddress" placeholder="Address"></textarea>

			<input type="button" name="previous" class="previous action-button"
				value="Previous" /> <input type="button" name="next"
				class="next action-button" value="Next" />
				
				</fieldset>
				<fieldset>
    <h2 class="fs-title">payment Details</h2>
    <h3 class="fs-subtitle">with high security</h3>

    <input type="text" name="Visa" placeholder="Visa" />
    
    <input type="text" name="MasterCard" placeholder="Master card Number" />
    
	<input type="text" name="bankATM" placeholder="other bank ATM number" />
	
	<input type="text" name="money" placeholder="Amount" />	
	
	
    <input type="button" name="previous" class="previous action-button" value="Previous" />
			<!-- <input type="submit" class="submit action-button" name="submit" value="Book"> -->
			<input type="button" value="Book" onclick="bookingRequest()">
		</fieldset>
		
		<%} %>
</form>
	
</body>
	<script
		src='//production-assets.codepen.io/assets/common/stopExecutionOnTimeout-b2a7b3fe212eaa732349046d8416e00a9dec26eb7fd347590fbced3ab38af52e.js'></script>
	<script
		src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
	<script
		src='//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
	<script>
		//jQuery time
		var current_fs, next_fs, previous_fs; //fieldsets
		var left, opacity, scale; //fieldset properties which we will animate
		var animating; //flag to prevent quick multi-click glitches

		$(".next").click(
				function() {
					if (animating)
						return false;
					animating = true;

					current_fs = $(this).parent();
					next_fs = $(this).parent().next();

					//activate next step on progressbar using the index of next_fs
					$("#progressbar li").eq($("fieldset").index(next_fs))
							.addClass("active");

					//show the next fieldset
					next_fs.show();
					//hide the current fieldset with style
					current_fs.animate({
						opacity : 0
					}, {
						step : function(now, mx) {
							//as the opacity of current_fs reduces to 0 - stored in "now"
							//1. scale current_fs down to 80%
							scale = 1 - (1 - now) * 0.2;
							//2. bring next_fs from the right(50%)
							left = (now * 50) + "%";
							//3. increase opacity of next_fs to 1 as it moves in
							opacity = 1 - now;
							current_fs.css({
								'transform' : 'scale(' + scale + ')',
								'position' : 'absolute'
							});
							next_fs.css({
								'left' : left,
								'opacity' : opacity
							});
						},
						duration : 800,
						complete : function() {
							current_fs.hide();
							animating = false;
						},
						//this comes from the custom easing plugin
						easing : 'easeInOutBack'
					});
				});

		$(".previous").click(
				function() {
					if (animating)
						return false;
					animating = true;

					current_fs = $(this).parent();
					previous_fs = $(this).parent().prev();

					//de-activate current step on progressbar
					$("#progressbar li").eq($("fieldset").index(current_fs))
							.removeClass("active");

					//show the previous fieldset
					previous_fs.show();
					//hide the current fieldset with style
					current_fs.animate({
						opacity : 0
					}, {
						step : function(now, mx) {
							//as the opacity of current_fs reduces to 0 - stored in "now"
							//1. scale previous_fs from 80% to 100%
							scale = 0.8 + (1 - now) * 0.2;
							//2. take current_fs to the right(50%) - from 0%
							left = ((1 - now) * 50) + "%";
							//3. increase opacity of previous_fs to 1 as it moves in
							opacity = 1 - now;
							current_fs.css({
								'left' : left
							});
							previous_fs.css({
								'transform' : 'scale(' + scale + ')',
								'opacity' : opacity
							});
						},
						duration : 800,
						complete : function() {
							current_fs.hide();
							animating = false;
						},
						//this comes from the custom easing plugin
						easing : 'easeInOutBack'
					});
				});

		
		//# sourceURL=pen.js
	</script>
	
	<script type="text/javascript">
	
	function bookingRequest()
	{
		
		alert("in booking request");
		document.fname.action="bookingAction.jsp";
		document.fname.submit();
	}
	</script>

</html>