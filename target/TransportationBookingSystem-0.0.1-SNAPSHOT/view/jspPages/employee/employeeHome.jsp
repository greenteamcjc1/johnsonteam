<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px;}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 600px;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: 800px;
        padding: 15px;
      }
      .row.content {height: 600px;} 
    }
    
    #section1{ padding-top:50px;height:800px;color: black; background-color: white; }
    
    #section2{ padding-top:50px;height:800px;color: black; background-color: white; }
  
    
    
    
    
  </style>
</head>
<body>

<%response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0); %>


<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>TRANSPORTATION BOOKING SYSTEM</h4>
      <ul class="nav nav-pills nav-stacked">
        <li><a href="#section1" data-toggle="tooltip" data-placement="top" title="Company Details">Company Details</a></li>
        <li><a href="#section2" data-toggle="tooltip" data-placement="top" title="Owner Infomation">Owner Infomation</a></li>
        <li><a href="empProfile.tiles" data-toggle="tooltip" data-placement="top" title="MyProfile">MyProfile</a></li>
        <li><a href="#section2" data-toggle="tooltip" data-placement="top" title="EdteProfile">EdteProfile</a></li>
      </ul><br>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Blog..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div>
    </div>
   
   
       <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
       
        
       
        
        <div id="section1" class="container-fluid" >
            <div class="container" >
                <div class="col-sm-8 text-left">
                    <div class="col-md-9 col-lg-9">
                        <div class="page_title text-center">
                            <h2 style="margin-left: 300px;">Our Company</h2>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8 text-left">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="single_blog">
                            <div class="blog_img">
                               <a href="#"> <img src="img/blog/blog_one.jpg" alt=""></a>
                                <div class="blog_date">
                                    <p class="Blog_month">24 <span>June</span></p>
                                    <p class="blog_year">2015</p>
                                </div>
                            </div>
                            <div class="blog_content blog_page">   
                                <p class="blog_name">By  /  John Smith  /  In Packing Tips</p>
                                <h3><a href="#">Its mission to explore strange new worlds to seek out new life and new</a></h3>
                                <p>Just two good ol' boys Never meanin' no harm. Beats all you've ever saw been in trouble with the law since the day they was born. You wanna be where you can see our troubles are all the same. You wanna be where everybody knows Your name. Boy the way Glen Miller played. </p>
                            </div>
                        </div>
                    </div>
                  
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="single_blog">
                            <div class="blog_img">
                               <a href="#"><img src="img/blog/blog_two.jpg" alt=""></a>
                                <div class="blog_date">
                                    <p class="Blog_month">24 <span>June</span></p>
                                    <p class="blog_year">2015</p>
                                </div>
                            </div>
                            <div class="blog_content blog_page">   
                                <p class="blog_name">By  /  John Smith  /  In Packing Tips</p>
                                <h3><a href="#">These are the voyages of the Starship Enterprise So lets this beautiful day</a></h3>
                                <p>Took a whole lotta tryin' just to get up that hill? They're creepy and they're kooky mysterious and spooky. They're all together ooky the Addams Family. The Love Boat soon will be making another run. The Love Boat promises something for everyone.</p>
                            </div>
                        </div>
                    </div>
                   
                
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="single_blog">
                            <div class="blog_img">
                                <a href="#"><img src="img/blog/blog_three.jpg" alt=""></a>
                                <div class="blog_date">
                                    <p class="Blog_month">24 <span>June</span></p>
                                    <p class="blog_year">2015</p>
                                </div>
                            </div>
                            <div class="blog_content blog_page">   
                                <p class="blog_name">By  /  John Smith  /  In Packing Tips</p>
                                <h3><a href="#">that started from this tropic port aboard this tiny ship the last Battlestar</a></h3>
                                <p>I guess well never know. Its like a kind of torture to have to watch the show? So get a witch's shawl on a broomstick you can crawl on. Were gonna pay a call on the Addams Family. I guess well never know. Its like a kind of torture to have to watch the show? So get a witch's shawl on a broomstick you can crawl on.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="single_blog">
                            <div class="blog_img">
                                <a href="#"><img src="img/blog/blog_four.jpg" alt=""></a>
                                <div class="blog_date">
                                    <p class="Blog_month">24 <span>June</span></p>
                                    <p class="blog_year">2015</p>
                                </div>
                            </div>
                            <div class="blog_content blog_page">   
                                <p class="blog_name">By  /  John Smith  /  In Packing Tips</p>
                                <h3><a href="#">Never heard the word impossible This time there's no stopping us.</a></h3>
                                <p>Till the one day when the lady met this fellow and they knew it was much more than a hunch. And when the odds are against him and their dangers work to do. You bet your life Speed Racer he will see it through. Texas tea. Till the one day when the lady met this fellow and they knew it was much more than a hunch. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="single_blog">
                            <div class="blog_img">
                               <a href="#"><img src="img/blog/blog_five.jpg" alt=""></a>
                                <div class="blog_date">
                                    <p class="Blog_month">24 <span>June</span></p>
                                    <p class="blog_year">2015</p>
                                </div>
                            </div>
                            <div class="blog_content blog_page">   
                                <p class="blog_name">By  /  John Smith  /  In Packing Tips</p>
                                <h3><a href="#">We finally got a piece of the pie Got kind of tired packin' and unpackin' </a></h3>
                                <p>If not for the courage of the fearless crew the Minnow would be lost. the Minnow would be lost. Sunny Days sweepin' the clouds away. On my way to where the air is sweet. Can you tell me how to get how to get to Sesame Street. If not for the courage of the fearless crew the Minnow would be lost. the Minnow would be lost. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Blog Area-->
        <!-- Newsletter Area -->
        </div></div>
<div id="section2" class="container-fluid">

                <div class="container">
                   <div class="col-sm-15 text-left">
                        <div class="col-md-15 col-lg-15 col-sm-20 col-xs-20">
                            <div class="barner_text text-center">
                                <h2>moveing overseas</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            

        <!--End Slider Area-->
        <!--Start Sercice Details Area -->
        <section class="service_details_area section_padding">
            <div class="container">
                <div  class="col-sm-15 text-left">
                    <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                        <div class="service_details_page_content">
                           <img src="img/service/service_1.jpg" alt="">
                           <h3>Moving Overseas</h3>
                           <p>Just two good ol' boys Wouldn't change if they could. Fightin' the system like a true modern day Robin Hood. All of them had hair of gold like their mother the youngest one in curls. As long as we live its you and me baby. There ain't nothin' wrong with that. Doin' it our way. There's nothing we wont try. Never heard the word impossible. This time there's no stopping us. Doin' it our way. Nothin's gonna turn us back now. Straight ahead and on the track now. We're gonna make our dreams come true. Goodbye gray sky hello blue. There's nothing can hold me when I hold you. Feels so right it cant be wrong. Rockin' and rollin' all week long.</p>
                           <div class="service_details_bottom_content">
                               <img class="alignright" src="img/service/service_2.jpg" alt="">
                               <p>Mister we could use a man like Herbert Hoover again. Maybe you and me were never meant to be. But baby think of me once in awhile. I'm at WKRP in Cincinnati! Just two good ol' boys Wouldn't change if they could. Fightin' the system like a true modern day Robin Hood. </p>
                               <ul>
                                   <li>The first mate and his Skivery best to make the others comfortable in their tropic</li>
                                   <li>Michael Knight a young loner on a crusade to champion the cause of the innocent</li>
                                   <li>The powerless in a world of criminals who operate above the law.</li>
                                   <li>You would see the biggest gift would be from me and the card attached would</li>
                                   <li>These are the voyages of the Starship Enterprise</li>
                               </ul>
                           </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
                        <div class="get_quate_sidebar">
                            <div class="s_sidebar">
                                <h3>our services</h3>
                                <ul class="quate_sidebar">
                                    <li class="quate_icon_1"><a href="">moving locally or interstate</a></li>
                                    <li class="quate_icon_2"><a href="">moving overseas</a></li>
                                    <li class="quate_icon_3"><a href="">corporate relocation</a></li>
                                    <li class="quate_icon_4"><a href="">commercial relocation</a></li>
                                    <li class="quate_icon_5"><a href="">packing</a></li>
                                    <li class="quate_icon_6"><a href="">storage</a></li>
                                </ul>
                                <p><a href="get-a-quate.html">GET A QUATE</a></p>
                                <div class="sidebar_img">
                                    <img src="img/faq/faq_1.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Sercice Details Area-->
        <!-- Newsletter Area-->
        <section class="newsletter_area">
            <div class="container">
                <div  class="col-sm-15 text-left">
                    <div class="col-md-9 col-lg-9">
                       <div class="signup_newsletter">
                           <p>singup for our</p>
                           <h3>newsletter</h3>
                           <form action="process.php">
                               <input type="email" placeholder="e-mail address" required>
                               <input type="submit" value="suscribe">
                           </form>
                       </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Newsletter Area-->
 


</div></div>


</body>
</html>
