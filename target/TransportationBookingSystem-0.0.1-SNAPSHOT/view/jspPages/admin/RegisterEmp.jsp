<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head></head>

<body>
	<div id="wrapper">

		<div id="page-inner">


			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="card-title">
								<div class="title">
									Employee registration

<script type="text/javascript"> 

 
											function display(){
	
													document.employee.action="EmployeeDisplay";
													document.employee.submit();
												}

											function add(){
	
													document.employee.action="addEmployee.tiles";
													document.employee.submit();
												}
											
											
											function update(){
												
												document.employee.action="updateEmployee.tiles";
												document.employee.submit();
											}
											
											function remove(){
												
												document.employee.action="deleteEmployee.tiles";
												document.employee.submit();
											}



									</script>
									

									
									<form name="employee">
										<button type="submit" class="btn btn-default" onclick="display()"
											style="margin-left: 600px">Display Employee</button>
										

										<button type="submit" class="btn btn-default" onclick="add()"
											style="margin-left: 5px">Add Employee</button>
										 
										
									</form>

								</div>
							</div>
						</div>
						<div class="panel-body">
							<form class="form-inline" action="register" method="post">
								<div class="form-group" style="margin-left: 200px;">
									<table>
										<tr>
											<td><label>First Name</label></td>
											<td><input type="text" name="fname" class="form-control"
												id="exampleInputName2" placeholder="First Name"></td>
										</tr>

										<tr>
											<td><label>Last Name</label></td>
											<td><input type="text" name="lname" class="form-control"
												id="exampleInputName2" placeholder="Last Name"></td>
										<tr>

											<td><label>Password</label></td>
											<td><input type="password" name="password"
												class="form-control" id="exampleInputName2"
												placeholder="********"></td>
										</tr>

										<tr>
											<td><label>Mobile</label></td>
											<td><input type="text" name="mobile"
												class="form-control" id="exampleInputName2"
												placeholder="Mobile Number"></td>
										</tr>

										<tr>
											<td><label>Address</label></td>
											<td><input type="text" name="address"
												class="form-control" id="exampleInputName2"
												placeholder="Address"></td>
										</tr>

										<tr>
											<td><label>Email </label></td>
											<td><input type="text" name="email" class="form-control"
												id="exampleInputName2" placeholder="Email"></td>
												
												<input type="hidden" name="userType" value="employee"/>
										</tr>
											
											  	
										<tr>
												<td> <button type="submit" class="btn btn-default" style="margin-top: 20px;  width: 100px;">Submit</button></td>
												<td> <button type="submit" class="btn btn-default" style="margin-top: 20px; margin-left: 20px; width: 100px; ">Reset</button></td>
										</tr>
										
										<tr>
												
										</tr>
									</table>

								</div>

							</form>
						</div>

					</div>
				</div>

			</div>

		</div>
		<!--  /. PAGE INNER  -->
	</div>
	<!-- /. WRAPPER -->

</body>
</html>