package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.model.VehiclePojo;
import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EmployeeSearchBoxServlet extends HttpServlet {
	
	
	/**
	 * 
	 * @author Ajit
	 * Date:-26/Aug/2017
	 * Discription:-Custmer Menegment section--Multiple Data Serach Box
	 *  Modul:-Employee
	 */
	
	
	
	
	
	

	private String emailRegx="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private String Vehicle_numberRegx="^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$";
	private String BookingID="^[0-9]{4}$";
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		   HashMap<String,Object>JSON=new HashMap<>();
		   Gson gson=new GsonBuilder().setPrettyPrinting().create();
		   response.setContentType("application/json");
		/**
		 * Email Validation
		 */
		Pattern pattern=Pattern.compile(emailRegx);
		Matcher matcher=pattern.matcher(request.getParameter("getMsg"));
		boolean Email=matcher.matches();
		System.out.println(Email);
		
		/**
		 * Vehicle_Number Validation
		 */

		Pattern pattern2=Pattern.compile(Vehicle_numberRegx);
		Matcher matcher2=pattern2.matcher(request.getParameter("getMsg"));
		boolean Vehicle_Number=matcher2.matches();
		
		/**
		 * Booking_Id Validation
		 */
		
		Pattern pattern3=Pattern.compile(BookingID);
		Matcher matcher3=pattern3.matcher(request.getParameter("getMsg"));
		boolean Booking_ID=matcher3.matches();
		
		HttpSession session=request.getSession(false);
		
		if(session!=null)
		{
			if(Email==true)
			{
				BookingPojo bookingPojo=new BookingPojo();
				bookingPojo.setEmail(request.getParameter("getMsg"));
				System.out.println(bookingPojo.getEmail());
				EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
				ArrayList<Object>arrayList=serviceImplementation.searchEmailDetails(request, bookingPojo);
				
				
				if(arrayList.size()!=0)
				{
					
					arrayList.add(0,"Valid Email");
					arrayList.add(1,"1");
					JSON.put("Result","OK");
               	    JSON.put("Records",arrayList);
         		    String jsonArray=gson.toJson(JSON);
         	        response.getWriter().print(jsonArray);  
					
				}
				else{
                    arrayList.add(0,"Invalid Email");
					JSON.put("Result","OK");
               	    JSON.put("Records",arrayList);
         		    String jsonArray=gson.toJson(JSON);
         	        response.getWriter().print(jsonArray);  
					
				}
				
				
			}else if(Vehicle_Number)
			{
				VehiclePojo vehiclePojo=new VehiclePojo();
				vehiclePojo.setVEHICLE_NUMBER(request.getParameter("getMsg"));
				EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
				ArrayList<Object>arrayList=serviceImplementation.searchVehicleDetails(request, vehiclePojo);
				
				JSON.put("Result","OK");
           	    JSON.put("Records",arrayList);
     		    String jsonArray=gson.toJson(JSON);
     	        response.getWriter().print(jsonArray);  
				
				
				
				
			}else if(Booking_ID)
			{
				
				BookingPojo bookingPojo=new BookingPojo();
				bookingPojo.setBooking_Id(Integer.parseInt(request.getParameter("getMsg")));
				EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
				ArrayList<Object>arrayList=serviceImplementation.searchBookingIdDetails(request, bookingPojo);
				
				JSON.put("Result","OK");
           	    JSON.put("Records",arrayList);
     		    String jsonArray=gson.toJson(JSON);
     	        response.getWriter().print(jsonArray);  
				
				
			}else{
				//invalid user
			}
			
			
			
			
			
			
			
			
		}else{request.getRequestDispatcher("employee.tiles").forward(request, response); }
		
		
	}
	
}
