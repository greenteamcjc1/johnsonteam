package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;





public class EmployeeGetRegisteredDataBookingServlet extends HttpServlet{
      
	
	
	/**
	 * 
	 * @author Ajit
	 * Date:-23/Aug/2017
	 * Discription:-Collect data from registered reord table
	 *  Modul:-Employee
	 */
	
	
	
	
	
	
	
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		                   
		HashMap<String,Object>JSON=new HashMap<>();
		System.out.println(request.getParameter("email"));
		
                   HttpSession session=request.getSession(false);
                   
                   if(session!=null)
                   {
                	   RegisterPojo registerPojo=new RegisterPojo();
                	   registerPojo.setEmail(request.getParameter("email"));
                	   
                	   EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
                	   ArrayList<Object>arrayList=serviceImplementation.registeredgetdataBooking(request, registerPojo);
                	  
                	   Gson gson=new GsonBuilder().setPrettyPrinting().create();
            		   response.setContentType("application/json");
                	   
                		   
                		   JSON.put("Result","OK");
                      	   JSON.put("Records",arrayList);
                		   String jsonArray=gson.toJson(JSON);
                	     response.getWriter().print(jsonArray);
                	   
                   }else{request.getRequestDispatcher("homeDef.jsp").include(request, response);}
                   
                   
		
		
	}
}
