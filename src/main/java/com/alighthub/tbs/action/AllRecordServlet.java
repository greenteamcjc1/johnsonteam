package com.alighthub.tbs.action;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AllRecordServlet extends HttpServlet {

	
	
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * @author Ajit
	 * Date:-26/Aug/2017
	 * Discription:-Get all data-registered data and unregistered data
	 * Modul:-Employee
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HashMap<String,Object>JSON=new HashMap<>();
		HttpSession session=request.getSession(false);
		
		if(session!=null)
		{
		
			BookingPojo bookingPojo=new BookingPojo();
			bookingPojo.setMessages(request.getParameter("getMsg"));
			 EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
			 ArrayList<BookingPojo> arrayList=serviceImplementation.allRecord(bookingPojo,request);
			
			 Gson gson=new GsonBuilder().setPrettyPrinting().create();
			   response.setContentType("application/json");
			
			   JSON.put("Result","OK");
        	   JSON.put("Records",arrayList);
        	   String jsonArray=gson.toJson(JSON);
      	       response.getWriter().print(jsonArray);
			
		}else{request.getRequestDispatcher("employee.tiles").forward(request, response);}
		
		
		
		
	}
	
	
}
