package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;
import com.alighthub.tbs.util.Booking_Confermation_Mailer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class OrderActivationServlet extends HttpServlet {

	/**
	 * 
	 * @author Ajit
	 *  Date:-2/Sept/2017
	 * Discription:-Fare And Vechicle Number Set
	 *  Modul:-Employee
	 */

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ArrayList<Object>arrayList=new ArrayList<>();
		HashMap<String,Object>JSON=new HashMap<>();

		String booking_ID=request.getParameter("Booking_ID");
		String vehicle_Number=request.getParameter("Vehicle_Number");
		String fare=request.getParameter("Fare");
        String email= request.getParameter("Email");
        String deliver_Address=request.getParameter("Deliver_Address");
          
        String Messages="Hii\nYour Booking_ID Is:-"+booking_ID+"\nThe goods will be delivered at:-"+deliver_Address+"\nThe Respective VehicleNumber:-"+vehicle_Number+"\nTotal FareIs:-"+fare+"\n\nThanku...Visiteagain";
        
		//System.out.println(request.getParameter("totalamount"));

		HttpSession session=request.getSession(false);

		Gson gson=new GsonBuilder().setPrettyPrinting().create();
		response.setContentType("application/json");


		if(session!=null)
		{
			if(booking_ID.equals("") || vehicle_Number.equals("") || fare.equals("") )
			{

				arrayList.add(0,"Check: BookingID VehicleNumber TotalDistance");
				JSON.put("Result","OK");
				JSON.put("Records",arrayList);
				String jsonArray=gson.toJson(JSON);
				response.getWriter().print(jsonArray);
				
				
			} 
			else  
			{               
				BookingPojo bookingPojo=new BookingPojo();
				bookingPojo.setBooking_Id(Integer.parseInt(booking_ID));
				bookingPojo.setVehicle_Number(vehicle_Number);
				bookingPojo.setFare(fare);
				bookingPojo.setEmail(email);

				EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
				arrayList=serviceImplementation.orderActivation(arrayList, bookingPojo, request);
                    
				

				JSON.put("Result","OK");
				JSON.put("Records",arrayList);
				String jsonArray=gson.toJson(JSON);
				response.getWriter().print(jsonArray);
				
				bookingPojo.setEmail(bookingPojo.getEmail());
				bookingPojo.setMessages(Messages);
				Booking_Confermation_Mailer.sendEmail(bookingPojo); 
				//System.out.println("done");

			}

		}else{request.getRequestDispatcher("employee.tiles").forward(request, response);}

	}

}