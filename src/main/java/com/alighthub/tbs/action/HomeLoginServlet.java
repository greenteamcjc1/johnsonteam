package com.alighthub.tbs.action;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.LoginPojo;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.serviceImplementation.HomeServiceImplementation;
/**
 * 
 * @author Ajit
 *  Date:-21/Aug/2017
 * Discription:-Login----admin employee customer
 * Modul:-Employee
 */
public class HomeLoginServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static Logger logger=Logger.getLogger(HomeLoginServlet.class.getName());
	 
	HttpSession session=null;
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		if(session!=null)
		{
			
		  doPost(request, response);
		}
		else
		{
			
			request.getRequestDispatcher("homeDef.jsp").forward(request, response);
		}
		
	}
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 System.out.println(" HOME LOGIN-===cahe clear--->>>");
		   response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
		
		session=request.getSession(false);
		if(session!=null)
		{
		
		System.err.println("LOGIN SEVLET SESSION ID------>>>"+session.getId());
	
		
		LoginPojo loginPojo=new LoginPojo();
        loginPojo.setUsername(request.getParameter("usrname"));
		loginPojo.setPassword(request.getParameter("password"));
		
		
		
		HomeServiceImplementation homeServiceImplementation=new HomeServiceImplementation();
		loginPojo=homeServiceImplementation.loginCheck(loginPojo, request);
		
		System.out.println("****"+loginPojo.getUser_type());
		
		if(loginPojo.getUser_type().equalsIgnoreCase("admin"))
		{
			request.getRequestDispatcher("admin.tiles").include(request,response);
		}
		else if(loginPojo.getUser_type().equalsIgnoreCase("employee"))
		{  
			RegisterPojo registerPojo=new  RegisterPojo();
			//set user type
			registerPojo.setUserType(loginPojo.getUser_type());
			//set email for geting all data
			registerPojo.setEmail(loginPojo.getUsername());
			
			HomeServiceImplementation homeServiceImplementation2=new HomeServiceImplementation();
			
			registerPojo=homeServiceImplementation2.getEmployeeDetails(registerPojo, request);
			//HttpSession session=request.getSession(true);
			session.setAttribute("Employee_Fname", registerPojo.getFirstName());
			session.setAttribute("Employee_Lname", registerPojo.getLastName());
			session.setAttribute("Employee_MobNo", registerPojo.getMobileNo());
			session.setAttribute("Employee_Address", registerPojo.getAddress());
			session.setAttribute("Employee_Email", registerPojo.getEmail());
			
			//System.out.println("&*&*&*&"+(String)session2.getAttribute("Employee_Fname")+(String)session2.getAttribute("Employee_Lname")+(String)session2.getAttribute("Employee_MobNo")+(String)session2.getAttribute("Employee_Address")+(String)session2.getAttribute("Employee_Email"));
			System.out.println( "**************"+registerPojo.getFirstName()+registerPojo.getLastName()+registerPojo.getMobileNo()+registerPojo.getAddress()+registerPojo.getEmail());
			
			request.getRequestDispatcher("employee.tiles").include(request,response);
		}
		else if(loginPojo.getUser_type().equalsIgnoreCase("customer"))
		{
			


			HttpSession session=request.getSession(false);

			session.setAttribute("username",loginPojo.getUsername() );
            RequestDispatcher td=request.getRequestDispatcher("tp.tiles");
			td.forward(request, response);
			
			
			
			//request.getRequestDispatcher("#").include(request, response);
		}
		else if(loginPojo.getUser_type().equalsIgnoreCase("InvalidUser"))
		{
			request.getRequestDispatcher("homeDef.jsp").include(request, response);;
		}
		else{
			request.getRequestDispatcher("homeDef.jsp").include(request, response);;
		}
		}
		}
		
		
	
		
	
	
}
