package com.alighthub.tbs.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.model.Feedbackpojo;
import com.alighthub.tbs.serviceImplementation.FeedbackserviceImplementation;

public class FeedbackServlet extends HttpServlet {

	
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
			/**
			 * getting values from feedbackpage
			 * 
			 */
			String fname=request.getParameter("names");
			String email=request.getParameter("emails");
			String mobileno=request.getParameter("mobileno");
			String feedback=request.getParameter("feedback");
			String NoOfFeedback=request.getParameter("star");
			
			System.out.println(fname+" "+email+" "+mobileno+" "+feedback+" "+NoOfFeedback);
			
			
			Feedbackpojo feedbackpojo=new Feedbackpojo();
			
			feedbackpojo.setName(fname);
			feedbackpojo.setEmail(email);
			feedbackpojo.setMobileno(mobileno);
			feedbackpojo.setFeedback(feedback);
			feedbackpojo.setFeedbackNo(NoOfFeedback);
			
			
			FeedbackserviceImplementation feedbackserviceImplementation=new FeedbackserviceImplementation();
			feedbackserviceImplementation.feedbackservice(feedbackpojo,request);
			
	}
	
	
	
}
