package com.alighthub.tbs.action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.serviceImplementation.HomeServiceImplementation;
/**
 * 
 * @author Ajit
 *  Date:-21/Aug/2017
 *  Discription:-Home RegistrationPage GetData
 * Modul:-Employee
 */
public class HomeRegistrationServlet extends HttpServlet {
	
	
	HttpSession session=null;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		if(session==null)
		{
			
		  doPost(request, response);
		}
		else
		{
			
			request.getRequestDispatcher("homeDef.jsp").forward(request, response);
		}
		
	}
	
	
	
	private static final long serialVersionUID = 1L;

	static Logger logger=Logger.getLogger(HomeRegistrationServlet.class.getName());
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		      System.out.println("HOME REGISTRATION  cahe clear--->>>");
		   /* response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);*/
		
		  session=request.getSession(false);
		  
		  System.err.println("REGISTRATION SESSION ID------>>>"+session.getId());
			
		  if(session!=null)
		  {
		
		try
		{
	     
		/**
		 * RegisterPage Data Get And Set Pojo
		 */
	
		RegisterPojo registerPojo=new RegisterPojo();
		registerPojo.setFirstName(request.getParameter("Firstname"));		
		registerPojo.setLastName(request.getParameter("Lastname"));
		registerPojo.setMobileNo(request.getParameter("MobileNo"));
		registerPojo.setAddress(request.getParameter("Address"));
		registerPojo.setEmail(request.getParameter("Email"));
		registerPojo.setPassword(request.getParameter("Password"));
		registerPojo.setUserType(request.getParameter("UserType"));
		/*
		System.out.println(request.getParameter("Firstname"));
		System.out.println(request.getParameter("Lastname"));
		System.out.println(request.getParameter("MobileNo"));
		System.out.println(request.getParameter("Address"));
		System.out.println(request.getParameter("Email"));
		System.out.println(request.getParameter("Password"));
		System.out.println(request.getParameter("UserType"));
		*/
		/**
		 * Pojo Object Pass In HomeServiceImplementation
		 */
		
		HomeServiceImplementation homeServiceImplementation=new HomeServiceImplementation();
		boolean flag=homeServiceImplementation.registerData_Insert(registerPojo,request);
		
		
		
		if(flag)
		{
			
			request.getRequestDispatcher("ClientErrorMsg.jsp").include(request,response);
		}
		else
		{
			request.getRequestDispatcher("homeDef.jsp").include(request,response);
			
		}
		
		}
		catch(Exception e)
		{
			logger.error(e);
			request.getRequestDispatcher("ClientErrorMsg.jsp").include(request,response);
		}
	   }
		  else{
			  request.getRequestDispatcher("homeDef.jsp").include(request,response);
		  }
		  
			
	}
	
		
	
	
}
