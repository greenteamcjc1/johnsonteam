package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.dao.DriverDao;
import com.alighthub.tbs.model.DriverPojo;
import com.alighthub.tbs.serviceImplementation.DriverMangmentServiceImpli;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DriverMangmentServlet extends HttpServlet{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private HashMap<String, Object> JSONROOT = new HashMap<String, Object>();

	private DriverDao dao;

	DriverMangmentServiceImpli impli=new DriverMangmentServiceImpli();
		public DriverMangmentServlet() 
		{
		dao = new DriverDao();
		}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		List<DriverPojo> Driver = new ArrayList<DriverPojo>();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		response.setContentType("application/json");

		if (action != null) {
			try {
				if (action.equals("list")) {
					DriverMangmentServiceImpli impli=new DriverMangmentServiceImpli();
					Driver = impli.AllDriver();

					// Return in the format required by jTable plugin
					JSONROOT.put("Result", "OK");
					JSONROOT.put("Records", Driver);

					// Convert Java Object to Json
					String jsonArray = gson.toJson(JSONROOT);

					response.getWriter().print(jsonArray);
				} else if (action.equals("create") || action.equals("update")) {
					DriverPojo driverPojo = new DriverPojo();

					if (request.getParameter("firstname") != null) {
						String firstname = request.getParameter("firstname");
						driverPojo.setFirstname(firstname);
					}

					
					if (request.getParameter("lastname") != null) {
						String lastname = request.getParameter("lastname");
						driverPojo.setLastname(lastname);
					}

					if (request.getParameter("dob") != null) {
						String dob = request.getParameter("dob");
						driverPojo.setDob(dob);
					}

					if (request.getParameter("adhar_number") != null) {
						String adhar_number = request.getParameter("adhar_number");
						driverPojo.setAdhar_number(adhar_number);
					}

					if (request.getParameter("licence_number") != null) {
						String licence_number = request.getParameter("licence_number");
						driverPojo.setLicence_number(licence_number);
					}

					if (request.getParameter("address") != null) {
						String address = request.getParameter("address");
						driverPojo.setAddress(address);
					}
			

					if (request.getParameter("mobile_Number") != null) {
						String mobile_Number = request.getParameter("mobile_Number");
						driverPojo.setMobile_Number(mobile_Number);
					}

					if (request.getParameter("vechicle_number") != null) {
						String vechicle_number = request.getParameter("vechicle_number");
						driverPojo.setVechicle_number(vechicle_number);
					}
		
					if (action.equals("create")) {
						
						impli.createDriver(driverPojo);
						
					} else if (action.equals("update")) {
						
					DriverMangmentServiceImpli impli=new DriverMangmentServiceImpli();
						impli.updateDriver(driverPojo);
					}

					// Return in the format required by jTable plugin
					JSONROOT.put("Result", "OK");
					JSONROOT.put("Record", driverPojo);

					// Convert Java Object to Json
					String jsonArray = gson.toJson(JSONROOT);
					response.getWriter().print(jsonArray);
				} else if (action.equals("delete")) {
					// Delete record
					if (request.getParameter("firstname") != null) {
						
						String firstname= request.getParameter("firstname");
						impli.deleteDriver(firstname);

						// Return in the format required by jTable plugin
						JSONROOT.put("Result", "OK");

						// Convert Java Object to Json
						String jsonArray = gson.toJson(JSONROOT);
						response.getWriter().print(jsonArray);
					}
				}
			} catch (Exception ex) {
				JSONROOT.put("Result", "ERROR");
				JSONROOT.put("Message", ex.getMessage());
				String error = gson.toJson(JSONROOT);
				response.getWriter().print(error);
			}
		}
	}

	
		
		
		
	}

