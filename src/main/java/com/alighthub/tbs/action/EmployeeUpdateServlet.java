package com.alighthub.tbs.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeUpdateImplementation;

public class EmployeeUpdateServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	System.out.println("In Upadate Servlet");
		String firstName=request.getParameter("fname");
		String lastName=request.getParameter("lname");
		String mobile=request.getParameter("mobile");
		String address=request.getParameter("address");
		String email=request.getParameter("email");
		 
		RegisterPojo registerPojo=new RegisterPojo();
		
			registerPojo.setFirstName(firstName);
			registerPojo.setLastName(lastName);
			registerPojo.setMobileNo(mobile);
			registerPojo.setAddress(address);
			registerPojo.setEmail(email);
			
			
			
			EmployeeUpdateImplementation implementation=new EmployeeUpdateImplementation();
			int x=implementation.updateEmployeeRecord(registerPojo, request);
			
			if(x>0)
			{	
				 request.getRequestDispatcher("EmployeeDisplay").forward(request, response);
			}
			
			else
			{
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
			
	}		

}
