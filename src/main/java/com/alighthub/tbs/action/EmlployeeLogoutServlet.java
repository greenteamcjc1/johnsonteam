package com.alighthub.tbs.action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;

public class EmlployeeLogoutServlet extends HttpServlet {

	
	/**
	 * 
	 * @author Ajit
	 * Date:-23/Aug/2017
	 * Discription:-Employee Logout
	 *  Modul:-Employee
	 */
	
	
	
	
	HttpSession session=null;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		if(session!=null)
		{
			
		  doPost(request, response);
		}
		else
		{
			
			request.getRequestDispatcher("homeDef.jsp").forward(request, response);
		}
		
	}
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//PrintWriter out =response.getWriter();
		 System.out.println("EMPLOYEE LOGOUT===cahe clear--->>>");
		    response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
		
	  session =request.getSession(false);
	  System.err.println("CONTROLLER LOGOUT SESSION ID------>>>"+session.getId());
	
	  if(session!=null)
	  {
		System.out.println("&*&*&*&"+(String)session.getAttribute("Employee_Fname")+(String)session.getAttribute("Employee_Lname")+(String)session.getAttribute("Employee_MobNo")+(String)session.getAttribute("Employee_Address")+(String)session.getAttribute("Employee_Email"));
		
		EmployeeServiceImplementation employeeServiceImplementation=new EmployeeServiceImplementation();
		session=employeeServiceImplementation.employeeLogOut(session);
		
		request.getRequestDispatcher("homeDef.jsp").include(request, response);
	  }
	  else
	  {
		  request.getRequestDispatcher("employee.tiles").include(request, response);
	  }
	  
	}
	
		
	
	
}
