package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.serviceImplementation.DriverDisplayServiceImplementation;
import com.alighthub.tbs.serviceImplementation.EmployeeDispayServiceImplementation;
 
public class DriverDisplayServlet extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		 DriverDisplayServiceImplementation implementation=new DriverDisplayServiceImplementation();
	List<DriverPojo1> list= implementation.getDriverData(request);
	
		 
		/*for(RegisterPojo pojo:list)
		{
			
			System.out.println(pojo.getFirstName());
			
		}
			*/
	
		request.setAttribute("driverData", list);
		request.getRequestDispatcher("displayDriver.tiles").forward(request, response);
		
		
	}
	

	
	
	
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	  
/*		HttpSession session=request.getSession(); 
		if(! session.isNew())
		{
			session.invalidate();
			session=request.getSession(true);
			doPost(request, response);
		}
		else
		{
				response.setHeader("cache-control","no-cache, no-store, must-revalidate");
				response.setHeader("Pragma", "no-cache");
				response.setDateHeader("Expires", 0);	
			  request.getRequestDispatcher("home.tiles").forward(request, response);
			  
		}
*/
		
		doPost(request, response);
		
	}

	
}
