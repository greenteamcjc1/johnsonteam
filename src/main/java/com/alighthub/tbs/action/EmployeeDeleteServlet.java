package com.alighthub.tbs.action;

import java.io.IOException;
 
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.model.DriverPojo1;
 import com.alighthub.tbs.serviceImplementation.DriverDeleteServiceImplementation;
 
public class EmployeeDeleteServlet  extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String adharNumber=request.getParameter("selectRecord");
		

			
		adharNumber.equals(adharNumber);
		DriverPojo1 driverPojo=new DriverPojo1();
		driverPojo.setAdharNumber(adharNumber);
		
		DriverDeleteServiceImplementation  implementation=new DriverDeleteServiceImplementation();
		int x=implementation.deleteDriverRecord(driverPojo, request);
	
		if(x>0)
		{	
			 request.getRequestDispatcher("driverDisplay").forward(request, response);
		}
		
		else
		{
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}
		
	}
	

	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
			doPost(request, response);
		
	 	}
	

}
