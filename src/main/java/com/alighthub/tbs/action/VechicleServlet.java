package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.dao.VechileDao;
import com.alighthub.tbs.model.VechiclePojo;
import com.alighthub.tbs.serviceImplementation.VechicleServiceImpli;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
public class VechicleServlet extends HttpServlet {


		
     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

			private HashMap<String, Object> JSONROOT = new HashMap<String, Object>();

			private VechileDao dao;

			VechicleServiceImpli impli=new VechicleServiceImpli();
			public VechicleServlet() {
				dao = new VechileDao();
			}

			protected void doGet(HttpServletRequest request,
					HttpServletResponse response) throws ServletException, IOException {
				doPost(request, response);
			}

			protected void doPost(HttpServletRequest request,
					HttpServletResponse response) throws ServletException, IOException {

				String action = request.getParameter("action");
				List<VechiclePojo> Vechicle = new ArrayList<VechiclePojo>();
				
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				response.setContentType("application/json");

				if (action != null) {
					try {
						if (action.equals("list")) {
							VechicleServiceImpli impli=new VechicleServiceImpli();
							Vechicle = impli.AllVechicle();

							// Return in the format required by jTable plugin
							JSONROOT.put("Result", "OK");
							JSONROOT.put("Records", Vechicle);

							// Convert Java Object to Json
							String jsonArray = gson.toJson(JSONROOT);

							response.getWriter().print(jsonArray);
						} else if (action.equals("create") || action.equals("update")) {
							VechiclePojo vechiclePojo= new VechiclePojo();
							
							if (request.getParameter("Number") != null) {
								String Number = request.getParameter("Number");
								vechiclePojo.setNumber(Number);
							}

							
							if (request.getParameter("modelNo") != null) {
								String ModelNo = request.getParameter("modelNo");
								vechiclePojo.setModelNo(ModelNo);
							}

							if (request.getParameter("insurenceNo") != null) {
								String InsurenceNo = request.getParameter("insurenceNo");
								vechiclePojo.setInsurenceNo(InsurenceNo);
							}

							if (request.getParameter("averege_speed") != null) {
								String Averege_speed = request.getParameter("averege_speed");
								vechiclePojo.setAverege_speed(Averege_speed);
							}

							if (request.getParameter("owner") != null) {
								String Owner = request.getParameter("owner");
								vechiclePojo.setOwner(Owner);
							}

							if (request.getParameter("permetNo") != null) {
								String PermetNo = request.getParameter("permetNo");
								vechiclePojo.setPermetNo(PermetNo);
							}
							
							if (request.getParameter("rc_copy") != null) {
								String Rc_copy = request.getParameter("rc_copy");
								vechiclePojo.setRc_copy(Rc_copy);
							}

							if (request.getParameter("vechicle_type") != null) {
								String Vechicle_type = request.getParameter("vechicle_type");
								vechiclePojo.setVechicle_type(Vechicle_type);
							}

							if (request.getParameter("other") != null) {
								String Other = request.getParameter("other");
								vechiclePojo.setOther(Other);
							}
				
							if (action.equals("create")) {
								
								impli.createVechicle(vechiclePojo);
								
							} else if (action.equals("update")) {
								
							VechicleServiceImpli impli=new VechicleServiceImpli();	
								impli.updateVechicle(vechiclePojo);
							}

							// Return in the format required by jTable plugin
							JSONROOT.put("Result", "OK");
							JSONROOT.put("Record", vechiclePojo);

							// Convert Java Object to Json
							String jsonArray = gson.toJson(JSONROOT);
							response.getWriter().print(jsonArray);
						} else if (action.equals("delete")) {
							// Delete record
							if (request.getParameter("Number") != null) {
								
								String Number = request.getParameter("Number");
								impli.deleteVechicle(Number);

								// Return in the format required by jTable plugin
								JSONROOT.put("Result", "OK");

								// Convert Java Object to Json
								String jsonArray = gson.toJson(JSONROOT);
								response.getWriter().print(jsonArray);
							}
						}
					} catch (Exception ex) {
						JSONROOT.put("Result", "ERROR");
						JSONROOT.put("Message", ex.getMessage());
						String error = gson.toJson(JSONROOT);
						response.getWriter().print(error);
					}
				}
			}
}