package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class FareCalculationServlet extends HttpServlet {

	/**
	 * 
	 * @author Ajit 
	 * Date:-3/Sept/2017
	 * Discription:-Total Amount Calculation Using Total Distance
	 *  Modul:-Employee
	 * 
	 */
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	  HttpSession session=request.getSession(false);
	
	  if(!session.isNew())
	  {
		
		  BookingPojo bookingPojo=new BookingPojo();
		  bookingPojo.setTotalDistance(Integer.parseInt(request.getParameter("distance")));
		  bookingPojo.setWeight(request.getParameter("weight"));
		  EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
		  BookingPojo bookingPojo2=serviceImplementation.AmountCalculation(request, bookingPojo);
		   
		  HashMap<String,Object>JSON=new HashMap<>();
		   Gson gson=new GsonBuilder().setPrettyPrinting().create();
		   response.setContentType("application/json");
		   
		   JSON.put("Result","OK");
    	   JSON.put("Records",bookingPojo2);
    	   String jsonArray=gson.toJson(JSON);
  	       response.getWriter().print(jsonArray);
		  
	  }else{request.getRequestDispatcher("employee.tiles").forward(request, response);}
	  
		
	}
	
	
	
	
}
