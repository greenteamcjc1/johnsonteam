package com.alighthub.tbs.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.model.DriverPojo;
import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.serviceImplementation.DriverRegistrationServiceImplementation;
import com.alighthub.tbs.serviceImplementation.EmployeeRegistrationServiceImplementation;

public class DriverRegistrationServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String firstName=request.getParameter("fname");
		String lastName=request.getParameter("lname");
		String dob=request.getParameter("dob");
		String adhar=request.getParameter("adharNumber");
		String licenceNumber=request.getParameter("licenceNumber");
		String address=request.getParameter("address");
		String mobileNo=request.getParameter("mobile");
		String vehicleNumber=request.getParameter("vehicleNumber");
		
		
		 
	
		
		/**
		 * 
		 * Set the values in the Registerpojo class fields
		 * 
		 */
		
		
		DriverPojo1 driverPojo=new DriverPojo1();
		driverPojo.setFirstName(firstName);
		driverPojo.setLastName(lastName);
		driverPojo.setDOB(dob);
		driverPojo.setAdharNumber(adhar);
		driverPojo.setLicenceNumber(licenceNumber);
		driverPojo.setAddress(address);
		driverPojo.setMobileNumber(mobileNo);
		driverPojo.setVehicle_number(vehicleNumber);
		
		DriverRegistrationServiceImplementation implementation=new DriverRegistrationServiceImplementation();
	boolean b=	implementation.registerDriver(driverPojo, request);
	
	if(b)
	{
		request.getRequestDispatcher("error.jsp").forward(request, response);
	}
	else{
		request.getRequestDispatcher("displayDriver.tiles").forward(request, response);
	}
		

	}
}
