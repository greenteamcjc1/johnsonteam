package com.alighthub.tbs.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;

public class EmpRegisteredBookingServlet extends HttpServlet{
	
	
	
	/**
	 * 
	 * @author Ajit
	 * Date:-28/Aug/2017
	 * Discription:-Registered Booking  and set data in registered table
	 *  Modul:-Employee
	 * 
	 */
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		HttpSession session=request.getSession(false);
		
		if(session!=null)
		{
		
        BookingPojo bookingPojo=new BookingPojo();
		
		bookingPojo.setFirst_Name(request.getParameter("fname"));
		bookingPojo.setLast_Name(request.getParameter("lname"));
		bookingPojo.setMobile_Number(request.getParameter("mobileno"));
		bookingPojo.setAddress(request.getParameter("address"));
		bookingPojo.setEmail(request.getParameter("email"));
		
		bookingPojo.setBooking_Date(request.getParameter("bookingdate"));
		bookingPojo.setRequired_Date(request.getParameter("deliverdate"));
		bookingPojo.setRequired_Time(request.getParameter("delivertime"));
		bookingPojo.setSource(request.getParameter("placeFrom"));
		bookingPojo.setDestination(request.getParameter("placeTO"));
		bookingPojo.setVehicle_Number(request.getParameter("vehicle_no"));
		bookingPojo.setFare(request.getParameter("Fare"));
		bookingPojo.setWeight(request.getParameter("weight"));
		bookingPojo.setNumberOf_Vehicle(request.getParameter("no_ofvehicle"));
		bookingPojo.setDelivered_Address(request.getParameter("deliveraddress"));
		System.out.println(bookingPojo.getFare());
		EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
	    boolean report=serviceImplementation.registeredBooking(bookingPojo, request);
		
		if(report)
		{
			request.getRequestDispatcher("ClientErrorMsg.jsp").include(request, response);
			
		}else{request.getRequestDispatcher("employee.tiles").include(request, response);}
		
		
		
		
		}else{request.getRequestDispatcher("employee.tiles").forward(request, response);}
		
		
	}

}
