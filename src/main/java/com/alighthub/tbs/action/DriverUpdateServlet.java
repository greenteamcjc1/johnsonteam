package com.alighthub.tbs.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.model.DriverPojo1;
 import com.alighthub.tbs.serviceImplementation.DriverUpdateImplementation;
import com.alighthub.tbs.serviceImplementation.EmployeeUpdateImplementation;

public class DriverUpdateServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		 
		String firstName=request.getParameter("fname");
		String lastName=request.getParameter("lname");
		String dob=request.getParameter("dob");
		String adharNumber=request.getParameter("adhar_number");
		String licenceNumber=request.getParameter("lNumber");
		String address=request.getParameter("address"); 
		String mobileNumber=request.getParameter("mobile");
		String vehicleNumber=request.getParameter("vNumber");
		
		System.out.println("In Update Driver");
		System.out.println(adharNumber);
		
		DriverPojo1 driverPojo=new DriverPojo1();
		
			driverPojo.setFirstName(firstName);
			driverPojo.setLastName(lastName);
			driverPojo.setDOB(dob);
			driverPojo.setAdharNumber(adharNumber);
			driverPojo.setLicenceNumber(licenceNumber);
			driverPojo.setAddress(address);
			driverPojo.setMobileNumber(mobileNumber);
			driverPojo.setVehicle_number(vehicleNumber);
			
			
			
			
			DriverUpdateImplementation implementation=new DriverUpdateImplementation();
			int x=implementation.updateDriverRecord(driverPojo, request);
			
			if(x>0)
			{	
				 request.getRequestDispatcher("driverDisplay").forward(request, response);
			}
			
			else
			{
				request.getRequestDispatcher("error.jsp").forward(request, response);
			}
			
			
	}		

}
