package com.alighthub.tbs.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeRegistrationServiceImplementation;

public class EmplyeeRegistrationServlet extends HttpServlet {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String firstName=request.getParameter("fname");
		String lastName=request.getParameter("lname");
		String password=request.getParameter("password");
		String mobileNo=request.getParameter("mobile");
		String address=request.getParameter("address");
		String email=request.getParameter("email");
		String userType=request.getParameter("userType");
		
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(password);
		System.out.println(mobileNo);
		System.out.println(address);
		System.out.println(email);
		System.out.println(userType);
	
	
		
		/**
		 * 
		 * Set the values in the Registerpojo class fields
		 * 
		 */
		
		
		RegisterPojo registerPojo=new RegisterPojo();
		registerPojo.setFirstName(firstName);
		registerPojo.setLastName(lastName);
		registerPojo.setPassword(password);
		registerPojo.setMobileNo(mobileNo);
		registerPojo.setAddress(address);
		registerPojo.setEmail(email);
		registerPojo.setUserType(userType);
		
		EmployeeRegistrationServiceImplementation implementation=new EmployeeRegistrationServiceImplementation();
		implementation.registerEmployee(registerPojo, request);
		

	}
}
