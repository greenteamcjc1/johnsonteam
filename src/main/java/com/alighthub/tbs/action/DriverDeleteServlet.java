package com.alighthub.tbs.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeDeleteServiceImplementation;
 
public class DriverDeleteServlet  extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	
	
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String email=request.getParameter("selectRecord");
		

			
		email.equals(email);
		RegisterPojo registerPojo=new RegisterPojo();
		registerPojo.setEmail(email);
		System.out.println(registerPojo.getEmail());
		
		EmployeeDeleteServiceImplementation  implementation=new EmployeeDeleteServiceImplementation();
		int x=implementation.deleteEmployeerecord(registerPojo, request);
	
		if(x>0)
		{	
			 request.getRequestDispatcher("removeDriver").forward(request, response);
		}
		
		else
		{
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}
		
	}
	

	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
			doPost(request, response);
		
	 	}
	

}
