package com.alighthub.tbs.action;

import java.awt.Font;
import java.awt.print.Book;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.model.ReportPojo;
import com.alighthub.tbs.serviceImplementation.EmployeeServiceImplementation;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;

public class GenerateReportServlet extends HttpServlet{
 
	
	/**
	 * 
	 * @author Ajit 
	 * Date:-2/Sept/2017
	 * Discription:-Customer ReportGenerate ////From Registered and Unregistered Table
	 *  Modul:-Employee
	 * 
	 */
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println(request.getParameter("Record"));
		System.out.println(request.getParameter("fromdate"));
		System.out.println(request.getParameter("todate"));
		
		HttpSession session=request.getSession(false);
		
		if(session!=null)
		{
			
			ReportPojo reportPojo=new ReportPojo();
			reportPojo.setMessages(request.getParameter("Record"));
			reportPojo.setFromDate(request.getParameter("fromdate"));
			reportPojo.setToDate(request.getParameter("todate"));
			EmployeeServiceImplementation serviceImplementation=new EmployeeServiceImplementation();
			ArrayList<Object>arrayList=serviceImplementation.generateReport(request, reportPojo);
			
			Document document=new Document(PageSize.A4);
			try {
				
				PdfWriter.getInstance(document,response.getOutputStream());
				
				document.open();
				
				Paragraph paragraph=new Paragraph("Transportation Booking System",FontFactory.getFont(FontFactory.TIMES_BOLD, 22,
						Element.ALIGN_CENTER, BaseColor.BLUE));
				
				paragraph.setAlignment(Element.ALIGN_CENTER);
				
				Paragraph paragraph2 = new Paragraph(
						"1st Floor, Above Sindhu Clinic, Gurudwara Chowk,",
						FontFactory.getFont(FontFactory.TIMES_ITALIC, 12,
								Element.ALIGN_CENTER));
				
				
				Paragraph paragraph3 = new Paragraph(
						"Akurdi,Pune-411033. Tel.:(020) 8485073229.",
						FontFactory.getFont(FontFactory.TIMES_ITALIC, 12,
								Element.ALIGN_CENTER));
				
				paragraph2.setAlignment(Element.ALIGN_CENTER);
				paragraph3.setAlignment(Element.ALIGN_CENTER);
				
				
				Paragraph paragraph4 = new Paragraph(
						"Custmer Details", FontFactory.getFont(
								FontFactory.TIMES_BOLDITALIC, 18,
								Font.CENTER_BASELINE, BaseColor.BLACK));
				paragraph4.setAlignment(Element.ALIGN_CENTER);

				document.add(paragraph);
				document.add(paragraph2);
				document.add(paragraph3);
				document.add(paragraph4);
				
	            Paragraph paragraph5=new Paragraph("---------------------------------------------------------------",
				
	            FontFactory.getFont(FontFactory.TIMES_BOLDITALIC, 24,
			    Font.CENTER_BASELINE));
				
	            paragraph5.setAlignment(Element.ALIGN_CENTER);
				document.add(paragraph5);
				
				
				
				PdfPTable Table=new PdfPTable(8);
				
				PdfPCell cell = new PdfPCell(new Paragraph("Booking ID",
						FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
								Element.ALIGN_CENTER)));
				
				cell.setFixedHeight(40);
				cell.setHorizontalAlignment(Element.ALIGN_CENTER);
				Table.setWidthPercentage(100);
				Table.addCell(cell);
                 
				
				
				 cell = new PdfPCell(new Paragraph("Email",
							FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
									Element.ALIGN_CENTER)));
					
					cell.setFixedHeight(40);
					cell.setHorizontalAlignment(Element.ALIGN_CENTER);
					Table.setWidthPercentage(100);
					Table.addCell(cell);
	                 
					
					 cell = new PdfPCell(new Paragraph("Booking Date",
								FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
										Element.ALIGN_CENTER)));
						
						cell.setFixedHeight(40);
						cell.setHorizontalAlignment(Element.ALIGN_CENTER);
						Table.setWidthPercentage(100);
						Table.addCell(cell);
		                 
						
						
						 cell = new PdfPCell(new Paragraph("Required Date",
									FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
											Element.ALIGN_CENTER)));
							
							cell.setFixedHeight(40);
							cell.setHorizontalAlignment(Element.ALIGN_CENTER);
							Table.setWidthPercentage(100);
							Table.addCell(cell);
			                 
							
							
							 cell = new PdfPCell(new Paragraph("Source",
										FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
												Element.ALIGN_CENTER)));
								
								cell.setFixedHeight(40);
								cell.setHorizontalAlignment(Element.ALIGN_CENTER);
								Table.setWidthPercentage(100);
								Table.addCell(cell);
				                 
								
								
								 cell = new PdfPCell(new Paragraph("Destination",
											FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
													Element.ALIGN_CENTER)));
									
									cell.setFixedHeight(40);
									cell.setHorizontalAlignment(Element.ALIGN_CENTER);
									Table.setWidthPercentage(100);
									Table.addCell(cell);
					                 
									
									
									 cell = new PdfPCell(new Paragraph("Deliver Address",
												FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
														Element.ALIGN_CENTER)));
										
										cell.setFixedHeight(40);
										cell.setHorizontalAlignment(Element.ALIGN_CENTER);
										Table.setWidthPercentage(100);
										Table.addCell(cell);
						                 
										
										
										 cell = new PdfPCell(new Paragraph("Total Amount",
													FontFactory.getFont(FontFactory.TIMES_ROMAN, 13,
															Element.ALIGN_CENTER)));
											
											cell.setFixedHeight(40);
											cell.setHorizontalAlignment(Element.ALIGN_CENTER);
											Table.setWidthPercentage(100);
											Table.addCell(cell);
							           
											
											for(Object object:arrayList)
										    {
												BookingPojo bookingPojo=(BookingPojo)object;
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getBooking_Id()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
												//System.out.println(bookingPojo.getEmail());
												
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getEmail()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
												
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getBooking_Date()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
												
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getRequired_Date()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
												
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getSource()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
												
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getDestination()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
												
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getDelivered_Address()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
												
												cell = new PdfPCell(new Paragraph("" + bookingPojo.getFare()));
												cell.setFixedHeight(40f);
												Table.addCell(cell);
												
											
											}
											
									document.add(Table)	;
									document.close();
			
									/*HashMap<String,Object>JSON=new HashMap<>();
									Gson gson=new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
									   response.setContentType("application/json");
									System.out.println("111111111111111111111111111111111111");
									   JSON.put("Result","OK");
									   System.out.println("222222222222222222222222222222222");
						        	   JSON.put("Records",document);
						        	   System.out.println("333333333333333333333333333333333");
						        	   String jsonArray=gson.toJson(JSON);
						        	   System.out.println("444444444444444444444444444444444");
						      	       response.getWriter().print(jsonArray);
						      	       System.out.println("555555555555555555555555555555555");
				
				*/
			} catch (DocumentException e) {
			
				e.printStackTrace();
			}
			
			
			
			
			
			
		}else{request.getRequestDispatcher("employee.tiles").forward(request, response);}
		
		
		
		
	}
	
}
