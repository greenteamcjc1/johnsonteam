package com.alighthub.tbs.dao;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.BookingPojo;

public class AmountCalculationDao {
	
	
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */

	public BookingPojo AmountCalculation(HttpServletRequest request, BookingPojo bookingPojo) {
		
	    int weight=Integer.parseInt(bookingPojo.getWeight());
		int totaldistance=bookingPojo.getTotalDistance();
		
		int perKm_price=0;
		int total_amount=0;
		if(weight<=6)
		{
			perKm_price=15;
			
		}else if(weight>6 && weight<=10)
		{
			perKm_price=20;
			
		}else if(weight>10 && weight<=15)
		{
			perKm_price=25;
			
		}else if(weight>15 && weight<=20)
		{
			perKm_price=30;
		}
		
		total_amount=weight*perKm_price*totaldistance;
		String fare=String.valueOf(total_amount);
		bookingPojo.setFare(fare);
		
		return bookingPojo;
	}
	
}
