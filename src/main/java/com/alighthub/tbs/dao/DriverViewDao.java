package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.DriverPojo1;
 import com.alighthub.tbs.util.Mailer;

public class DriverViewDao {
	
	static Logger logger=Logger.getLogger("DriverViewDao");
	
	
	public DriverPojo1 upadateDriverRecord( DriverPojo1 driverPojo,
			HttpServletRequest request) {
		
		
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("DBConnection");

		 
		String sql="select * from driver where ADHAR_NUMBER="+"'"+driverPojo.getAdharNumber()+"'";
		
		try {
			Statement statement=connection.createStatement();
			ResultSet resultSet=statement.executeQuery(sql);
			
			while(resultSet.next())
			{
				driverPojo.setFirstName(resultSet.getString(1));
				driverPojo.setLastName(resultSet.getString(2));
				driverPojo.setDOB(resultSet.getString(3));
				driverPojo.setAdharNumber(resultSet.getString(4));
				driverPojo.setLicenceNumber(resultSet.getString(5));
				driverPojo.setAddress(resultSet.getString(6));
				driverPojo.setMobileNumber(resultSet.getString(7));
				driverPojo.setVehicle_number(resultSet.getString(8));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e);
			Mailer.sendEmail();
			e.printStackTrace();
			
		}
		
		
		return driverPojo;
		
	}
	
	


}
