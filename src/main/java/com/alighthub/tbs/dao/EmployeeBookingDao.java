package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;

public class EmployeeBookingDao {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	public boolean employeeBooking(BookingPojo bookingPojo, HttpServletRequest request) {
		
		
	HttpSession session=request.getSession(false);
		
	Connection connection=(Connection)session.getAttribute("DBConnection");
	Properties properties=(Properties)session.getAttribute("properties");
	
	/**
	    * Generate Booking_Id
	    */
	    
	    String query=properties.getProperty("generate_Newbooking_id");
	    try{
	    	 int bookingID=5143;
	    	 Statement statement=connection.createStatement();
	    	 ResultSet resultSet=statement.executeQuery(query);
	    	   
	    	 while(resultSet.next())
	    	 {
	    		 bookingID=resultSet.getInt("BOOKING_ID");
	    	 }
	         bookingID=bookingID+101;
	         bookingPojo.setBooking_Id(bookingID);
	   
	    }catch (Exception e) {
			
	    	e.printStackTrace();
	    	
		}
	
	
	
	
	    /**
	     * Generate Booking count
	     */
	    
	    String query2=properties.getProperty("Newbooking_count");
	    try
	    {
	    	
	    	int count=0;
	    	PreparedStatement preparedStatement=connection.prepareStatement(query2);
	    	preparedStatement.setString(1,bookingPojo.getEmail());
	    	ResultSet resultSet=preparedStatement.executeQuery();
	    	
	    	if(resultSet.next())
	    	{
	    	         count=resultSet.getInt("COUNT");
	    	         count=count+1;
	    	         bookingPojo.setCount(count);
	    	}else{
	    		count=1;
	    		bookingPojo.setCount(count);
	    	}
	    	
	    }catch(Exception e)
	    {
	            e.printStackTrace();
	    }
	    
	
	 String query3=properties.getProperty("book_unregister_user");
      boolean report=true;
	
	
	try {
		PreparedStatement preparedStatement=connection.prepareStatement(query3);
		preparedStatement.setInt(1,bookingPojo.getBooking_Id());
		preparedStatement.setString(2,bookingPojo.getFirst_Name());
		preparedStatement.setString(3,bookingPojo.getLast_Name());
		preparedStatement.setString(4,bookingPojo.getMobile_Number());
		preparedStatement.setString(5,bookingPojo.getEmail());
		preparedStatement.setString(6,bookingPojo.getAddress());
		preparedStatement.setString(7,bookingPojo.getBooking_Date());
		preparedStatement.setString(8,bookingPojo.getRequired_Date());
		preparedStatement.setString(9,bookingPojo.getRequired_Time());
		preparedStatement.setString(10,bookingPojo.getSource());
		preparedStatement.setString(11,bookingPojo.getDestination());
		preparedStatement.setString(12,bookingPojo.getVehicle_Number());
		preparedStatement.setString(13,bookingPojo.getFare());
		preparedStatement.setString(14,bookingPojo.getWeight());
		preparedStatement.setString(15,bookingPojo.getNumberOf_Vehicle());
		preparedStatement.setString(16,bookingPojo.getDelivered_Address());
		preparedStatement.setInt(17,bookingPojo.getCount());
		
		report=preparedStatement.execute();
		
	} catch (SQLException e) {
		
		
		e.printStackTrace();
	}
	
	
		
		
		return report;
	}
	
	
	
	
	
	
}
