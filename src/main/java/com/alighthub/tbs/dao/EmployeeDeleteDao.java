package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.util.Mailer;

public class EmployeeDeleteDao {
	
	static Logger logger=Logger.getLogger("EmployeeDeleteDao");
	int x;

	public int deleteEmployeerecord(RegisterPojo registerPojo,HttpServletRequest request) {
		 
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("DBConnection");
		
		String sql1="delete from LOGIN_MASTER where USERNAME='"+registerPojo.getEmail()+"'";
		
		String sql2="delete from USER_REGISTRATION where EMAIL='"+registerPojo.getEmail()+"'";
		
			try {
				Statement st=connection.createStatement();
				st.executeUpdate(sql1);
				
				Thread.sleep(1000);
				
				Statement statement=connection.createStatement();
				 x=statement.executeUpdate(sql2);
				
				
			} catch (SQLException | InterruptedException e) {
				// TODO Auto-generated catch block
				logger.error(sql2);
				Mailer.sendEmail();
				e.printStackTrace();
				
			}
			
		
		return x;
	}

}
