package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.RegisterPojo;


public class EmpRegisteredGetDataBookingDao {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	public ArrayList<Object> registeredgetdataBooking(HttpServletRequest request, RegisterPojo registerPojo) {
		
		ArrayList<Object>arrayList=new ArrayList<>();
		
		HttpSession session=request.getSession(false);
		
		Connection connection=(Connection)session.getAttribute("DBConnection");
		Properties properties=(Properties)session.getAttribute("properties");
		
		  String query=properties.getProperty("login_getEmployeedetails");//-->> 7 number query
		  try {
			  
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1,registerPojo.getEmail());
			ResultSet resultSet=preparedStatement.executeQuery();
			
			if(resultSet.next())
			{     
				arrayList.add(0,"Valid Email-Id");
				registerPojo.setFirstName(resultSet.getString(1));
				registerPojo.setLastName(resultSet.getString(2));
				registerPojo.setMobileNo(resultSet.getString(3));
				registerPojo.setAddress(resultSet.getString(4));
				registerPojo.setEmail(resultSet.getString(5));
				arrayList.add(1,registerPojo);
			}else{
				
				arrayList.add(0,"InValid Email-Id");
				registerPojo.setFirstName("Invalid User");
				registerPojo.setLastName("Invalid User");
				registerPojo.setMobileNo("Invalid User");
				registerPojo.setAddress("Invalid User");
				registerPojo.setEmail("Invalid User");
				arrayList.add(1,registerPojo);
				
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return arrayList;
	}
}
