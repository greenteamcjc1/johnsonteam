package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;

public class NotificationDao {
	
	
	
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	

	ArrayList<Object>arrayList=new ArrayList<>();
	
public ArrayList<Object> Notification(HttpServletRequest request,BookingPojo bookingPojo) {

	  HttpSession session=request.getSession(false);
	  Connection connection=(Connection)session.getAttribute("DBConnection");
      Properties properties=(Properties)session.getAttribute("properties");
      
		String query=properties.getProperty("unregister_Notification");
		String query2=properties.getProperty("register_Notification");
		
		
		try {
			
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1,bookingPojo.getMessages());
			ResultSet resultSet=preparedStatement.executeQuery();
			int count=0;
			while(resultSet.next())
			{
				count=count+1;
			}
			bookingPojo.setCount(count);
		
			
			String para1=String.valueOf(bookingPojo.getCount());
			String text1="From UnRegistered Record-->"+para1+"-Customer Order Activation is pending";
			//msg 1 set
			arrayList.add(0,text1);
			
			
			PreparedStatement preparedStatement2=connection.prepareStatement(query2);
			preparedStatement2.setString(1,bookingPojo.getMessages());
			ResultSet resultSet2=preparedStatement2.executeQuery();
			int counts=0;
			BookingPojo bookingPojo2=new BookingPojo();
			while(resultSet2.next())
			{
				
				counts=counts+1;
				
			}
			bookingPojo2.setCount(counts);
			String para2=String.valueOf(bookingPojo2.getCount());
			String text2="From Registered Record-->"+para2+"-Customer Order Activation is pending";
			///msg 2 set
			arrayList.add(1,text2);
			int total=count+counts;
			String all=String.valueOf(total);
			
			arrayList.add(2,all);
			
			
			
		} catch (SQLException e) {
			
			
			e.printStackTrace();
		}
		
		
		
		return arrayList;
	}
	
	
}
