package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.alighthub.tbs.model.BookingPojo;


public class EmployeeRegisteredBookingDao {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	public boolean registeredBooking(BookingPojo bookingPojo, HttpServletRequest request) {
		
		
		   HttpSession session=request.getSession(false);
		   
		   Connection connection=(Connection)session.getAttribute("DBConnection");
		   Properties properties=(Properties)session.getAttribute("properties");
		  
		   /**
		    * Generate Booking_Id
		    */
		    
		    String query=properties.getProperty("generate_booking_id");
		    try{
		    	int bookingID=2143;
		    	Statement statement=connection.createStatement();
		    	 ResultSet resultSet=statement.executeQuery(query);
		    	   
		    	 while(resultSet.next())
		    	 {
		    		 bookingID=resultSet.getInt("BOOKING_ID");
		    	 }
		         bookingID=bookingID+101;
		         bookingPojo.setBooking_Id(bookingID);
		   
		    }catch (Exception e) {
				
		    	e.printStackTrace();
		    	
			}
		    
		    
		    /**
		     * Generate Booking count
		     */
		    
		    String query2=properties.getProperty("booking_count");
		    try
		    {
		    	
		    	int count=0;
		    	PreparedStatement preparedStatement=connection.prepareStatement(query2);
		    	preparedStatement.setString(1,bookingPojo.getEmail());
		    	ResultSet resultSet=preparedStatement.executeQuery();
		    	
		    	if(resultSet.next())
		    	{
		    	         count=resultSet.getInt("BOOKING_COUNT");
		    	         count=count+1;
		    	         bookingPojo.setCount(count);
		    	}else{
		    		count=1;
		    		bookingPojo.setCount(count);
		    	}
		    	
		    }catch(Exception e)
		    {
		            e.printStackTrace();
		    }
		    
		    
		    /**
		     * Insert in Table
		     */
		    
		    
		    String query3=properties.getProperty("book_registered");
		    boolean report=true;
		    try{
		    	
		    	PreparedStatement preparedStatement=connection.prepareStatement(query3);
		    	
		   
			     preparedStatement.setInt(1,bookingPojo.getBooking_Id());
			     preparedStatement.setString(2,bookingPojo.getVehicle_Number());
			     preparedStatement.setString(3,bookingPojo.getBooking_Date() ); 
			     preparedStatement.setString(4,bookingPojo.getRequired_Time() );
			     preparedStatement.setString(5,bookingPojo.getSource() );
			     preparedStatement.setString(6,bookingPojo.getDestination() );
			     preparedStatement.setString(7,bookingPojo.getWeight());
			     preparedStatement.setString(8,bookingPojo.getFare() );
			     preparedStatement.setString(9,bookingPojo.getEmail() );
			     preparedStatement.setString(10,bookingPojo.getDelivered_Address() );
			     preparedStatement.setString(11,bookingPojo.getNumberOf_Vehicle() );
			     preparedStatement.setString(12,bookingPojo.getRequired_Date());
			     preparedStatement.setInt(13,bookingPojo.getCount());
			     report=preparedStatement.execute();
			    
			
		    }catch (Exception e) {
				e.printStackTrace();
			}
		    
		
		return report;
			   
	}
	
	
	}
			     
			    
			     
			     
			  
			    
			     
		    	
			      
		   
		  
