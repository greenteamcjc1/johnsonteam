package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.alighthub.tbs.model.VechiclePojo;
import com.alighthub.tbs.util.DBconncection;




public class VechileDao {


	 Connection connection=DBconncection.DBconnection();
	 PreparedStatement pStmt=null;
	

	public void deleteVechicle(String number) 
	
	{

			String deleteQuery = "DELETE FROM VechicleManagement WHERE Number_N = ?";
			try {
				pStmt = connection.prepareStatement(deleteQuery);
				pStmt.setString(1, number);
				pStmt.executeUpdate();
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}		
		
	}

	public void createVechicle(VechiclePojo vechiclePojo)
	{
			String insertQuery = "INSERT INTO VechicleManagement(VEHICLE_NUMBER,MODEL_NUMBER,INSURANCE_NUMBER,AVERAGE_SPEED,OWNER,PERMIT_NUMBER,VEHICLE_TYPE,OTHER)values(?,?,?,?,?,?,?,?)";
			try {
				
				pStmt = connection.prepareStatement(insertQuery);
				
				pStmt.setString(1, vechiclePojo.getNumber());
				pStmt.setString(2, vechiclePojo.getModelNo());
				pStmt.setString(3, vechiclePojo.getInsurenceNo());
				pStmt.setString(4, vechiclePojo.getAverege_speed());
				pStmt.setString(5, vechiclePojo.getOwner());
				pStmt.setString(6, vechiclePojo.getPermetNo());
			    pStmt.setString(7, vechiclePojo.getVechicle_type());
				pStmt.setString(8, vechiclePojo.getOther());
				
				pStmt.executeUpdate();
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
		}

		
	public void updateVechicle(VechiclePojo vechiclePojo) 
	{
			String updateQuery = "UPDATE VechicleManagement SET (Number_N,ModelNo,InsurenceNo,Averege_speed,Owner,PermetNo,Rc_copy,Vechicle_type,Other)values(?,?,?,?,?,?,?,?,?)";
			try {
				
				pStmt = connection.prepareStatement(updateQuery);		
				
				pStmt.setString(1, vechiclePojo.getNumber());
				pStmt.setString(2, vechiclePojo.getModelNo());
				pStmt.setString(3, vechiclePojo.getInsurenceNo());
				pStmt.setString(4, vechiclePojo.getAverege_speed());
				pStmt.setString(5, vechiclePojo.getOwner());
				pStmt.setString(6, vechiclePojo.getPermetNo());
				pStmt.setString(7, vechiclePojo.getRc_copy());
				pStmt.setString(8, vechiclePojo.getVechicle_type());
				pStmt.setString(9, vechiclePojo.getOther());
				
				pStmt.executeUpdate();

			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
		}


	public List<VechiclePojo> AllVechicle() 
	{	
		List<VechiclePojo> Vechicle = new ArrayList<VechiclePojo>();

		System.out.println("in list");
		String query = "SELECT * FROM VechicleManagement";
		try {
			Statement stmt =connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			System.out.println("whileeee");
			while (rs.next()) {
				VechiclePojo vechiclePojo = new VechiclePojo();

				vechiclePojo.setNumber(rs.getString(1));
				vechiclePojo.setModelNo(rs.getString(2));
				vechiclePojo.setInsurenceNo(rs.getString(3));
				vechiclePojo.setAverege_speed(rs.getString(4));
				vechiclePojo.setOwner(rs.getString(5));
				vechiclePojo.setPermetNo(rs.getString(6));
				vechiclePojo.setRc_copy(rs.getString(7));
				vechiclePojo.setVechicle_type(rs.getString(8));
				vechiclePojo.setOther(rs.getString(9));
				Vechicle.add(vechiclePojo);
		
				System.out.println("in list last");
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return Vechicle;
	}
	
}