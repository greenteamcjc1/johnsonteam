package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
 

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.util.Mailer;

public class DriverRegistrationDao {
	
	static Logger logger=Logger.getLogger("DriverRegistrationDao");
	
	public boolean registerDriver(DriverPojo1 driverPojo,HttpServletRequest request )
	{
		
		boolean b=true;
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("DBConnection");
		String sql1="insert into driver(first_name,last_name,dob,adhar_number,licence_number,address,mobile_number,vehicle_number)values"
				+ "(?,?,?,?,?,?,?,? )";
		try {
			PreparedStatement  preparedStatement1=connection.prepareStatement(sql1);
			
			
			preparedStatement1.setString(1, driverPojo.getFirstName());
			preparedStatement1.setString(2, driverPojo.getLastName());
			preparedStatement1.setString(3, driverPojo.getDOB());
			preparedStatement1.setString(4, driverPojo.getAdharNumber());
			preparedStatement1.setString(5, driverPojo.getLicenceNumber());
			
			preparedStatement1.setString(6, driverPojo.getAddress());
			preparedStatement1.setString(7, driverPojo.getMobileNumber());
			preparedStatement1.setString(8, driverPojo.getVehicle_number());
		b=preparedStatement1.execute();
			 
		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e);
			Mailer.sendEmail();
			e.printStackTrace();
		}
			
			catch (Exception e) {
				logger.error(e);
				Mailer.sendEmail();
		}
		
		
	 
		System.out.println(b);
	return b;

	}

}
