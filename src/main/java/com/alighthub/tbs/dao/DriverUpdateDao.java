package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.DriverPojo1;
 
import com.alighthub.tbs.util.Mailer;

public class DriverUpdateDao {
	
	static Logger logger=Logger.getLogger("DriverUpdateDao");
	
	int x;
public int updateDriverRecord( DriverPojo1 driverPojo,	HttpServletRequest request) {
	 	
	HttpSession session=request.getSession();
	Connection connection=(Connection)session.getAttribute("DBConnection");


	/*String sql="update driver set first_name='"
			+driverPojo.getFirstName()
			+"', last_name='"+driverPojo.getLastName()
			+"', DOB='"+driverPojo.getDOB()
			 
			+"', LICENCE_NUMBER='"+driverPojo.getLicenceNumber()
			+"', ADDRESS='"+driverPojo.getAddress()
			+"', MOBILE_NUMBER='"+driverPojo.getMobileNumber()
			+"', VEHICLE_NUMBER='"+driverPojo.getVehicle_number()
			+"'  where ADHAR_NUMBER='"+driverPojo.getAdharNumber()
			+"'";
	
	
	
	*/
	
	String sql="update driver set first_name=?,last_name=?,DOB=?,LICENCE_NUMBER=?,ADDRESS=?,MOBILE_NUMBER=?,VEHICLE_NUMBER=? where ADHAR_NUMBER="+driverPojo.getAdharNumber();
			
		 
	
	//String sql="update USER_REGISTRATION set FIRST_NAME='uuu',LAST_NAME='llll',MOBILE_NUMBER='1111111',ADDRESS='dehu' where EMAIL='xxxxx@gmail.com'";
	

	try {
		PreparedStatement  preparedStatement1=connection.prepareStatement(sql);
		
		
		preparedStatement1.setString(1, driverPojo.getFirstName());
		preparedStatement1.setString(2, driverPojo.getLastName());
		preparedStatement1.setString(3, driverPojo.getDOB());
		//preparedStatement1.setString(4, driverPojo.getAdharNumber());
		preparedStatement1.setString(4, driverPojo.getLicenceNumber());
		
		preparedStatement1.setString(5, driverPojo.getAddress());
		preparedStatement1.setString(6, driverPojo.getMobileNumber());
		preparedStatement1.setString(7, driverPojo.getVehicle_number());
	 x=preparedStatement1.executeUpdate();
		
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		logger.error(e);
		Mailer.sendEmail();
		e.printStackTrace();
	}
	
		
	return x;
	}

 
}
