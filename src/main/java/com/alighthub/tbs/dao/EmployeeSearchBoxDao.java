package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.model.VehiclePojo;

public class EmployeeSearchBoxDao {

	
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	ArrayList<Object> arrayList=new ArrayList<>();
	

	public ArrayList<Object> searchEmailDetails(HttpServletRequest request, BookingPojo bookingPojo) {
		//System.out.println("service dao");
		  HttpSession session=request.getSession(false);
		  Connection connection=(Connection)session.getAttribute("DBConnection");
	      Properties properties=(Properties)session.getAttribute("properties");
	      
	     String query=properties.getProperty("Unregistered_getRecordEMAIL");
	     String query2=properties.getProperty("Registered_getRecordEMAIL");
	      
	      try {
	    	  
			 PreparedStatement preparedStatement=connection.prepareStatement(query);
			 preparedStatement.setString(1,bookingPojo.getEmail());
			 ResultSet resultSet=preparedStatement.executeQuery();
			
				PreparedStatement preparedStatement2=connection.prepareStatement(query2); 
				preparedStatement2.setString(1,bookingPojo.getEmail());
				ResultSet resultSet2=preparedStatement2.executeQuery();
				
					while(resultSet2.next())
					{
				     BookingPojo bookingPojo2=new BookingPojo();
					bookingPojo2.setBooking_Id(resultSet2.getInt(1));
					bookingPojo2.setVehicle_Number(resultSet2.getString(2));
					bookingPojo2.setBooking_Date(resultSet2.getString(3));
					bookingPojo2.setFare(resultSet2.getString(8));
					bookingPojo2.setDelivered_Address(resultSet2.getString(10));
					bookingPojo2.setRequired_Date(resultSet2.getString(12));
					bookingPojo2.setCount(resultSet2.getInt(13));
					arrayList.add(bookingPojo2);
					}
					
				 
				    while(resultSet.next())
				    {
				    	
				    	BookingPojo bookingPojo2=new BookingPojo();
				    	bookingPojo2.setBooking_Id(resultSet.getInt(1));
				    	bookingPojo2.setBooking_Date(resultSet.getString(7));
				    	bookingPojo2.setRequired_Date(resultSet.getString(8));
				    	bookingPojo2.setVehicle_Number(resultSet.getString(12));
				    	bookingPojo2.setFare(resultSet.getString(13));
				    	bookingPojo2.setDelivered_Address(resultSet.getString(16));
				    	bookingPojo2.setCount(resultSet.getInt(17));
				    	arrayList.add(bookingPojo2);
				    }
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		
		return arrayList ;
	}
	
	

	public ArrayList<Object> searchVehicleDetails(HttpServletRequest request, VehiclePojo vehiclePojo) {


		  HttpSession session=request.getSession(false);
		  Connection connection=(Connection)session.getAttribute("DBConnection");
	      Properties properties=(Properties)session.getAttribute("properties");
	      String query=properties.getProperty("Vechicle_getRecord");
	    
		
		   
			try {
				 PreparedStatement preparedStatement = connection.prepareStatement(query);
				 preparedStatement.setString(1,vehiclePojo.getVEHICLE_NUMBER());
				 ResultSet resultSet=preparedStatement.executeQuery();
				 
				 if(resultSet.next())
				 {
					 arrayList.add(0,"Valid VehicleNumber");
					 arrayList.add(1,"2");
					 vehiclePojo.setVEHICLE_NUMBER(resultSet.getString(1));
					 vehiclePojo.setMODEL_NUMBER(resultSet.getString(2));
					 vehiclePojo.setVEHICLE_TYPE(resultSet.getString(3));
					 vehiclePojo.setCAPACITY(resultSet.getInt(4));
					 vehiclePojo.setOWNER(resultSet.getString(5));
					 vehiclePojo.setINSURANCE_NUMBER(resultSet.getString(6));
					 vehiclePojo.setPERMIT_NUMBER(resultSet.getString(7));
					 vehiclePojo.setAVERAGE_SPEED(resultSet.getInt(9));
					 arrayList.add(2,vehiclePojo);
				 }
				 else{  arrayList.add(0,"InValid VehicleNumber");}
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		    
		      
		return arrayList;
	}
	
	

	public ArrayList<Object> searchBookingIdDetails(HttpServletRequest request, BookingPojo bookingPojo) {
		
		  HttpSession session=request.getSession(false);
	      Connection  connection=(Connection)session.getAttribute("DBConnection");
	      Properties properties=(Properties)session.getAttribute("properties");
		  
	      String query =properties.getProperty("unregistered_bookingidDetails");
	      String query2=properties.getProperty("registered_booingidDetails");
		    
	      try {
	    	  
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setInt(1,bookingPojo.getBooking_Id());
			ResultSet resultSet=preparedStatement.executeQuery();
			if(resultSet.next())
			{
				bookingPojo.setBooking_Id(resultSet.getInt(1));
				bookingPojo.setEmail(resultSet.getString(5));
				bookingPojo.setBooking_Date(resultSet.getString(7));
				bookingPojo.setRequired_Date(resultSet.getString(8));
				bookingPojo.setSource(resultSet.getString(10));
				bookingPojo.setDestination(resultSet.getString(11));
				bookingPojo.setWeight(resultSet.getString(14));
				bookingPojo.setDelivered_Address(resultSet.getString(16));
				
				arrayList.add(bookingPojo);
			}
			
			PreparedStatement preparedStatement2=connection.prepareStatement(query2);
			preparedStatement2.setInt(1, bookingPojo.getBooking_Id());
			ResultSet resultSet2=preparedStatement2.executeQuery();
			if(resultSet2.next())
			{
				bookingPojo.setBooking_Id(resultSet2.getInt(1));
				bookingPojo.setBooking_Date(resultSet2.getString(3));
				bookingPojo.setSource(resultSet2.getString(5));
				bookingPojo.setDestination(resultSet2.getString(6));
				bookingPojo.setWeight(resultSet2.getString(7));
				bookingPojo.setEmail(resultSet2.getString(9));
				bookingPojo.setDelivered_Address(resultSet2.getString(10));
				bookingPojo.setRequired_Date(resultSet2.getString(12));
				
				arrayList.add(bookingPojo);
			}
			Statement statement=connection.createStatement();
			String queru3=properties.getProperty("vehicle_Details");
			
			ResultSet resultSet3=statement.executeQuery(queru3);
			ArrayList<VehiclePojo>vehiclelist=new ArrayList<>();
			VehiclePojo vehiclePojo=null;
			while(resultSet3.next())
			{
				 vehiclePojo=new VehiclePojo();
				vehiclePojo.setVEHICLE_NUMBER(resultSet3.getString(1));
				vehiclelist.add(vehiclePojo);
			}
			
			if( 
			    arrayList.size()==1)
			{   arrayList.add(0,"Valid Booking_ID");
			    arrayList.add(1,"3");
				arrayList.add(2,vehiclelist);
				
			}else{
				
				bookingPojo.setBooking_Id(0000);
				bookingPojo.setBooking_Date("InValid Booking_ID");
				bookingPojo.setSource("InValid Booking_ID");
				bookingPojo.setDestination("InValid Booking_ID");
				bookingPojo.setWeight("InValid Booking_ID");
				bookingPojo.setEmail("InValid Booking_ID");
				bookingPojo.setDelivered_Address("InValid Booking_ID");
				bookingPojo.setRequired_Date("InValid Booking_ID");
				arrayList.add(bookingPojo);
				arrayList.add(0, "InValid Booking_ID");
				arrayList.add(1,"3");
				arrayList.add(2, vehiclelist);
				
			}
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		return arrayList;
	}
	
	
	
	
	
	
	
	
}
