package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.model.ReportPojo;

public class ReportGenerateDao {
	
	
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	
public ArrayList<Object> generateReport(HttpServletRequest request, ReportPojo reportPojo) {


	HttpSession session=request.getSession(false);
	ArrayList<Object>arrayList=new ArrayList<>();
	Connection connection=(Connection)session.getAttribute("DBConnection");
	Properties properties=(Properties)session.getAttribute("properties");
	
	if( reportPojo.getMessages().equals("registeredRecord") && reportPojo.getFromDate()==null && reportPojo.getToDate()==null)
	{
		String query=properties.getProperty("Registered_getRecord");
		try {
			
			Statement statement=connection.createStatement();
			ResultSet resultSet=statement.executeQuery(query);
			while(resultSet.next())
			{
				BookingPojo bookingPojo=new BookingPojo();
				bookingPojo.setBooking_Id(resultSet.getInt(1));
				bookingPojo.setBooking_Date(resultSet.getString(3));
				bookingPojo.setSource(resultSet.getString(5));
				bookingPojo.setDestination(resultSet.getString(6));
				bookingPojo.setFare(resultSet.getString(8));
				bookingPojo.setEmail(resultSet.getString(9));
				bookingPojo.setDelivered_Address(resultSet.getString(10));
				bookingPojo.setRequired_Date(resultSet.getString(12));
				arrayList.add(bookingPojo);
			}
			
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}else if(reportPojo.getMessages().equals("unregisteredRecord") && reportPojo.getFromDate()==null && reportPojo.getToDate()==null)
	{
		
		String query=properties.getProperty("Unregistered_getRecord");
		
		try {
			
			Statement statement = connection.createStatement();
		
		    ResultSet resultSet=statement.executeQuery(query);
		    
		while(resultSet.next())
		{
			BookingPojo bookingPojo=new BookingPojo();
			bookingPojo.setBooking_Id(resultSet.getInt(1));
			bookingPojo.setEmail(resultSet.getString(5));
			bookingPojo.setBooking_Date(resultSet.getString(7));
			bookingPojo.setRequired_Date(resultSet.getString(8));
			bookingPojo.setSource(resultSet.getString(10));
			bookingPojo.setDestination(resultSet.getString(11));
			bookingPojo.setFare(resultSet.getString(13));
			bookingPojo.setDelivered_Address(resultSet.getString(16));
			
			arrayList.add(bookingPojo);
		}
		
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
		
		
		
		
	}else if(reportPojo.getMessages().equals("registeredRecord"))
	{
		String query=properties.getProperty("registered_getData_BetwwenDate");
		
		try {
			
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1,reportPojo.getFromDate());
			preparedStatement.setString(2, reportPojo.getToDate());
			ResultSet resultSet=preparedStatement.executeQuery();
			while(resultSet.next())
			{
				
				BookingPojo bookingPojo=new BookingPojo();
				bookingPojo.setBooking_Id(resultSet.getInt(1));
				bookingPojo.setBooking_Date(resultSet.getString(3));
				bookingPojo.setSource(resultSet.getString(5));
				bookingPojo.setDestination(resultSet.getString(6));
				bookingPojo.setFare(resultSet.getString(8));
				bookingPojo.setEmail(resultSet.getString(9));
				bookingPojo.setDelivered_Address(resultSet.getString(10));
				bookingPojo.setRequired_Date(resultSet.getString(12));
				arrayList.add(bookingPojo);
				
			}
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		
		
	}else if(reportPojo.getMessages().equals("unregisteredRecord"))
	{
		
		
String query=properties.getProperty("registered_getData_BetwwenDate");
		
		try {
			
			PreparedStatement preparedStatement=connection.prepareStatement(query);
			preparedStatement.setString(1,reportPojo.getFromDate());
			preparedStatement.setString(2, reportPojo.getToDate());
			ResultSet resultSet=preparedStatement.executeQuery();
			while(resultSet.next())
			{
				
				BookingPojo bookingPojo=new BookingPojo();
				
				bookingPojo.setBooking_Id(resultSet.getInt(1));
				bookingPojo.setEmail(resultSet.getString(5));
				bookingPojo.setBooking_Date(resultSet.getString(7));
				bookingPojo.setRequired_Date(resultSet.getString(8));
				bookingPojo.setSource(resultSet.getString(10));
				bookingPojo.setDestination(resultSet.getString(11));
				bookingPojo.setFare(resultSet.getString(13));
				bookingPojo.setDelivered_Address(resultSet.getString(16));
				
				arrayList.add(bookingPojo);
				
			}
			
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
		
		
		
		
	}
	
	
	
	
		
		
		return arrayList;
	}
	

}
