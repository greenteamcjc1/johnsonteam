package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.util.Mailer;

public class EmployeeViewDao {
	
	static Logger logger=Logger.getLogger("EmployeeUpdateDao");
	
	
	public RegisterPojo upadateEmployeeRecord(RegisterPojo registerPojo,
			HttpServletRequest request) {
		
		
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("DBConnection");

		 String email=registerPojo.getEmail();
		String sql="select * from user_registration where email="+"'"+email+"'";
		
		try {
			Statement statement=connection.createStatement();
			ResultSet resultSet=statement.executeQuery(sql);
			
			while(resultSet.next())
			{
				registerPojo.setFirstName(resultSet.getString(1));
				registerPojo.setLastName(resultSet.getString(2));
				registerPojo.setMobileNo(resultSet.getString(3));
				registerPojo.setAddress(resultSet.getString(4));
				registerPojo.setEmail(resultSet.getString(5));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e);
			Mailer.sendEmail();
			e.printStackTrace();
			
		}
		
		
		return registerPojo;
		
	}
	
	


}
