package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.Feedbackpojo;

public class FeedbackDao {

	
	
	
	public void  feedbackservice(Feedbackpojo feedbackpojo,HttpServletRequest request)
	{
		
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("Connection");
		Properties properties=(Properties)session.getAttribute("properties");
		
		String sql=properties.getProperty("feedback_insert_query");
		
		try {
			
			PreparedStatement pst=connection.prepareStatement(sql);
			pst.setString(1, feedbackpojo.getName());
			pst.setString(2, feedbackpojo.getEmail());
			pst.setString(3, feedbackpojo.getMobileno());
			pst.setString(4, feedbackpojo.getFeedback());
			
			String ratingNo=feedbackpojo.getFeedbackNo();
			int Rating=Integer.parseInt(ratingNo);
			
			pst.setInt(5,Rating);
			pst.execute();
			System.out.println("done feedback");
			
		} catch (Exception e) {
			
		}
		
		
		
		
	}
	
	
	
	
}
