package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.alighthub.tbs.model.DriverPojo;
import com.alighthub.tbs.util.DBconncection;


public class DriverDao {

	 Connection connection=DBconncection.DBconnection();
	 PreparedStatement pStmt=null;
	
	
	public void createDriver(DriverPojo driverPojo) {


		String insertQuery = "INSERT INTO DriverManagment(firstname,lastname,dob,adhar_number,licence_number,address,mobile_number,Vechicle_number)values(?,?,?,?,?,?,?,?)";
		try {
			
			pStmt = connection.prepareStatement(insertQuery);
			
			pStmt.setString(1, driverPojo.getFirstname());
			pStmt.setString(2, driverPojo.getLastname());
			pStmt.setString(3, driverPojo.getDob());
			pStmt.setString(4, driverPojo.getAdhar_number());
			pStmt.setString(5, driverPojo.getLicence_number());
			pStmt.setString(6, driverPojo.getAddress());
			pStmt.setString(7, driverPojo.getMobile_Number());
			pStmt.setString(8, driverPojo.getVechicle_number());

			
			pStmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}

		
		
		
	}

	public void deleteDriver(String firstname) {

		String deleteQuery = "DELETE FROM DriverManagment WHERE firstname = ?";
		try {
			pStmt = connection.prepareStatement(deleteQuery);
			pStmt.setString(1, firstname);
			pStmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}		
		
	}

	public void updateDriver(DriverPojo driverPojo) {

		String updateQuery = "UPDATE DriverManagment SET (firstname,lastname,dob,adhar_number,licence_number,address,mobile_number,Vechicle_number)values(?,?,?,?,?,?,?,?)";
		try {
			
			pStmt = connection.prepareStatement(updateQuery);		
			
			pStmt.setString(1, driverPojo.getFirstname());
			pStmt.setString(2, driverPojo.getLastname());
			pStmt.setString(3, driverPojo.getDob());
			pStmt.setString(4, driverPojo.getAdhar_number());
			pStmt.setString(5, driverPojo.getLicence_number());
			pStmt.setString(6, driverPojo.getAddress());
			pStmt.setString(7, driverPojo.getMobile_Number());
			pStmt.setString(8, driverPojo.getVechicle_number());

			
			pStmt.executeUpdate();

		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		
	}

	public List<DriverPojo> AllDriver() {

		List<DriverPojo> Driver = new ArrayList<DriverPojo>();

		System.out.println("in list");
		String query = "SELECT * FROM DriverManagment";
		try {
			Statement stmt =connection.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			System.out.println("whileeee");
			while (rs.next()) {
				
				DriverPojo driverPojo=new DriverPojo();
				
				pStmt.setString(1, driverPojo.getFirstname());
				pStmt.setString(2, driverPojo.getLastname());
				pStmt.setString(3, driverPojo.getDob());
				pStmt.setString(4, driverPojo.getAdhar_number());
				pStmt.setString(5, driverPojo.getLicence_number());
				pStmt.setString(6, driverPojo.getAddress());
				pStmt.setString(7, driverPojo.getMobile_Number());
				pStmt.setString(8, driverPojo.getVechicle_number());
				
				pStmt.executeUpdate();
				System.out.println("in list last");
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
		return Driver;
	}

}
