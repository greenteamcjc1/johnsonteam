package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.util.Mailer;

public class EmployeeRegistrationDao {
	
	static Logger logger=Logger.getLogger("EmployeeRegistrationDao");
	
	public boolean registerEmployee(RegisterPojo registerPojo,HttpServletRequest request )
	{
		
		boolean b=true;
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("DBConnection");
		String sql1="insert into user_registration(first_name,last_name,mobile_number,address,email)values"
				+ "(?,?,?,?,? )";
		try {
			PreparedStatement  preparedStatement1=connection.prepareStatement(sql1);
			
			
			preparedStatement1.setString(1, registerPojo.getFirstName());
			preparedStatement1.setString(2, registerPojo.getLastName());
			preparedStatement1.setString(3, registerPojo.getMobileNo());
			preparedStatement1.setString(4, registerPojo.getAddress());
			preparedStatement1.setString(5, registerPojo.getEmail());
			preparedStatement1.execute();
			
			Thread.sleep(1000);
			
			String sql2="insert into login_master(username,password,user_type)values(?,?,?)";
			PreparedStatement preparedStatement2=connection.prepareStatement(sql2);
			
			preparedStatement2.setString(1, registerPojo.getEmail());
			preparedStatement2.setString(2, registerPojo.getPassword());
			preparedStatement2.setString(3, registerPojo.getUserType());
			b=preparedStatement2.execute();
 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			logger.error(e);
			Mailer.sendEmail();
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			logger.error(e);
			Mailer.sendEmail();
			e.printStackTrace();
		}  catch (Exception e) {
				logger.error(e);
				Mailer.sendEmail();
		}
		
		
	 
		System.out.println(b);
	return b;

	}

}
