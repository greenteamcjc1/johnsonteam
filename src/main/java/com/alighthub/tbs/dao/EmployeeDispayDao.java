package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.util.Mailer;

public class EmployeeDispayDao {
	
	static Logger logger=Logger.getLogger("EmployeeDispayDao");
	
	public List<RegisterPojo> getEmployeeData(HttpServletRequest request)
	{
		List<RegisterPojo> list=new ArrayList<RegisterPojo>();

		 
		
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("DBConnection");
		
		String sql="select u.first_name,u.last_name,u.MOBILE_NUMBER,u.ADDRESS,u.EMAIL from LOGIN_MASTER l  join USER_REGISTRATION u on l.USERNAME=u.EMAIL where l.USER_TYPE='employee'";

		Statement statement;
		try {
			
			statement = connection.createStatement();
			ResultSet resultSet=statement.executeQuery(sql);
			int i=0;
			while(resultSet.next())
			{
					RegisterPojo registerPojo=new RegisterPojo(); 
					
					registerPojo.setFirstName(resultSet.getString(1));
					registerPojo.setLastName(resultSet.getString(2));
					registerPojo.setMobileNo(resultSet.getString(3));
					registerPojo.setAddress(resultSet.getString(4));	
					registerPojo.setEmail(resultSet.getString(5));
					
					
				
					list.add(i,registerPojo);
					
					i++;
					
					 
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			Mailer.sendEmail();
		}
		
		
		
		
		return list;
	}

}
