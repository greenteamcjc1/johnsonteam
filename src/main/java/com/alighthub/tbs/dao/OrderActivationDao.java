package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;

public class OrderActivationDao {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	
	
	
	
	public ArrayList<Object> orderActivation(ArrayList<Object> arrayList, BookingPojo bookingPojo,
			HttpServletRequest request) {
		
		  HttpSession session=request.getSession(false);
	      Connection  connection=(Connection)session.getAttribute("DBConnection");
	      Properties properties=(Properties)session.getAttribute("properties");
		
		    String query=properties.getProperty("unregistered_update");
		    String quer2=properties.getProperty("registered_update");
		
		    int Message=0;
		
		    try {
		    	
				PreparedStatement preparedStatement=connection.prepareStatement(query);
				preparedStatement.setString(1,bookingPojo.getFare());
				preparedStatement.setString(2,bookingPojo.getVehicle_Number());
				preparedStatement.setInt(3,bookingPojo.getBooking_Id());
				
			    Message=preparedStatement.executeUpdate();//if update then retur 1	
				
			} catch (SQLException e) {
			
				e.printStackTrace();
			}
		    
		    
		    try {
		    	
				PreparedStatement preparedStatement=connection.prepareStatement(quer2);
				preparedStatement.setString(1,bookingPojo.getFare());
				preparedStatement.setString(2,bookingPojo.getVehicle_Number());
				preparedStatement.setInt(3,bookingPojo.getBooking_Id());
				
			    Message =preparedStatement.executeUpdate();
				
			} catch (SQLException e) {
				
				e.printStackTrace();
			}
		    
		    if(Message==1)
		    {
		    	  arrayList.add(0,"Booking Successfull");
		    }else{
		    	  arrayList.add(0,"Booking Successfull");
		    }
		    
		
		
		return arrayList;
	}
	
	
	
	
	
	
	
}
