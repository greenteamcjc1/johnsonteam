package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import com.alighthub.tbs.model.LoginPojo;
import com.alighthub.tbs.model.RegisterPojo;

public class HomeLoginDao {
	
	
	
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */

	static Logger logger=Logger.getLogger(HomeLoginDao.class.getName());

	
	public LoginPojo loginCheck(LoginPojo loginPojo, HttpServletRequest request) {
		
		  loginPojo.setUser_type("InvalidUser");
		
		try
		{
			
		HttpSession session=request.getSession(false);
		Connection connection=(Connection)session.getAttribute("DBConnection");
		
		Properties properties=(Properties)session.getAttribute("properties");
		//match user name and password
		String sql="select * from login_master where(username,password)in(('"+loginPojo.getUsername()+"','"+loginPojo.getPassword()+"'))";
		Statement statement=connection.createStatement();
		ResultSet resultSet=statement.executeQuery(sql);
		boolean flag=resultSet.next();
		System.out.println(flag);
		
		if(flag)//if flag is false
		{
			//get user type 
		  loginPojo.setUser_type(resultSet.getString(4));
		  System.out.println(resultSet.getString(4));
		  return loginPojo;
		}
		else
		{
			//invalid user name and password
			loginPojo.setUser_type("InvalidUser");
			return loginPojo;
		}
		
		
		}
		catch(Exception e)
		{
			logger.error(e);
		}
			
	
		return loginPojo;
	}
	
	
	
    public RegisterPojo getEmployeeDetails(RegisterPojo registerPojo,HttpServletRequest request) {
		
		
        	HttpSession session=request.getSession(false);
        	//get connection object
        	Connection connection=(Connection)session.getAttribute("DBConnection");
    		
    		Properties properties=(Properties)session.getAttribute("properties");
    		 //get properties data
    		String sql=properties.getProperty("login_getEmployeedetails");
    		 
    		
    		 try {
    			 
				PreparedStatement preparedStatement=connection.prepareStatement(sql);
				//set email match and get data
				preparedStatement.setString(1,registerPojo.getEmail());
				ResultSet resultSet=preparedStatement.executeQuery();
				//all data set in pojo
				while(resultSet.next())
				{
				registerPojo.setFirstName(resultSet.getString(1));
				registerPojo.setLastName(resultSet.getString(2));
				registerPojo.setMobileNo(resultSet.getString(3));
				registerPojo.setAddress(resultSet.getString(4));
				registerPojo.setEmail(resultSet.getString(5));
				}
				
			} catch (Exception e) {
				
				logger.error(e);
			}
		
		return registerPojo;
	}

	
	
}
