package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.Bookingpojo2;
import com.alighthub.tbs.util.Mailer;

public class BookingDao {

	int id;
	int booking_id=101;

	public void bookingservice(Bookingpojo2 bookingpojo,HttpServletRequest request,HttpServletResponse response) 
	{



		HttpSession session=request.getSession();

		Connection connection=(Connection)session.getAttribute("Connection");
		Properties properties=(Properties)session.getAttribute("properties");



		String sql1=properties.getProperty("booking_customer");

		String sql=properties.getProperty("retieve_booking_count");

		String customer_email=(String)session.getAttribute("username");

		System.out.println(customer_email);
		System.out.println("bookindao");



		try {

			int  count=0;
			PreparedStatement pst=connection.prepareStatement(sql);
			pst.setString(1, customer_email);

			ResultSet rs1=pst.executeQuery();
			System.out.println("before rs");

			if(rs1.next())
			{

				System.out.println("in rs");
				count=rs1.getInt("booking_count");
				System.out.println("out rs");
				System.out.println(count);

				count=count+1;

			}
			else{

				count=1;

			}



			/**
			 * 
			 *  generate booking ID
			 *  
			 */
			String query=properties.getProperty("generate_booking_id");
			try {


				Statement statement=connection.createStatement();
				ResultSet rs=statement.executeQuery(query);

				while(rs.next())
				{

					booking_id=rs.getInt(1);

				}
				booking_id+=1;
				bookingpojo.setBookingID(booking_id);

			} catch (Exception e) {

			}



			System.out.println("/////////////"+customer_email);

			PreparedStatement pst1=connection.prepareStatement(sql1);

			pst1.setInt(1, bookingpojo.getBookingID());
			pst1.setString(2, bookingpojo.getVehicleno());
			pst1.setString(3, bookingpojo.getDate());
			pst1.setString(4, bookingpojo.getDelivered_date());
			pst1.setString(5, bookingpojo.getTime());
			pst1.setString(6, bookingpojo.getPlaceform());
			pst1.setString(7, bookingpojo.getPlaceto());
			pst1.setString(8, bookingpojo.getWeight());
			pst1.setFloat(9, bookingpojo.getAmount());
			pst1.setString(10, bookingpojo.getEmail());


			pst1.setInt(11, count);
			pst1.execute();


			System.out.println("done2");

			//	Mailer.sendBookingId(bookingpojo,request);

			request.getRequestDispatcher("tp.tiles").forward(request, response);;


		} catch (Exception e) {

			e.printStackTrace();
		}



	}


}
