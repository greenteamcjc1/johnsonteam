package com.alighthub.tbs.dao;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;

/**
 * 
 * @author Ajit
 * Modul:-Employee
 */
public class AllRecordDao {
	
	

	 int count=0;
	ArrayList<BookingPojo> arrayList=new ArrayList();
	 
public ArrayList<BookingPojo> allRecord(BookingPojo bookingPojo, HttpServletRequest request) {
		
	  HttpSession session=request.getSession(false);
      Connection  connection=(Connection)session.getAttribute("DBConnection");
      Properties properties=(Properties)session.getAttribute("properties");
       
                   /**
                    * Get Data From Unregistered Table
                    */
      
              if(bookingPojo.getMessages().equals("Unregistered"))
              {
      
	      try {
	    	  String query=properties.getProperty("Unregistered_getRecord");
			Statement statement=connection.createStatement();
			 
			   ResultSet resultSet=statement.executeQuery(query);
			  
			   while(resultSet.next())
			   {
				  BookingPojo bookingPojo2=new BookingPojo();
				  bookingPojo2.setBooking_Id(resultSet.getInt(1));
				  bookingPojo2.setEmail(resultSet.getString(5));
				  bookingPojo2.setSource(resultSet.getString(10));
				  bookingPojo2.setDestination(resultSet.getString(11));
				  bookingPojo2.setVehicle_Number(resultSet.getString(12));
				  bookingPojo2.setFare(resultSet.getString(13)); 
				   
	                   arrayList.add(bookingPojo2);
	                   
			   }
			   
			   
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
              }
              
              /***
               * Get Data From Registered
               * 
               */
              
              
              else if(bookingPojo.getMessages().equals("Registered")){
            	  
            	  String query2=properties.getProperty("Registered_getRecord");
            	  
            	  try {
            		  
					Statement statement=connection.createStatement();
					ResultSet resultSet=statement.executeQuery(query2);
					
					while(resultSet.next())
					{
						BookingPojo  bookingPojo2=new BookingPojo();
						bookingPojo2.setBooking_Id(resultSet.getInt(1));
						bookingPojo2.setVehicle_Number(resultSet.getString(2));
						bookingPojo2.setSource(resultSet.getString(5));
						bookingPojo2.setDestination(resultSet.getString(6));
						bookingPojo2.setFare(resultSet.getString(8));
						bookingPojo2.setEmail(resultSet.getString(9));
					
						
						arrayList.add(bookingPojo2);
						
					}
					
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
            	  
            	  
            	  
              }
	
	
		return arrayList;
	}
	
	
}
