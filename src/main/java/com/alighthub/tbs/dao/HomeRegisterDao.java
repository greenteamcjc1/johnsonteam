package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;


import com.alighthub.tbs.model.RegisterPojo;

public class HomeRegisterDao {
	
	
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	

	static Logger logger=Logger.getLogger(HomeRegisterDao.class.getName());
	
	boolean flag=true;
    String insrtQuery1;
    String insrtQuery2;
	public boolean registerData_Insert(RegisterPojo registerPojo,HttpServletRequest request) {
		
		HttpSession session=request.getSession(false);
		
		Connection connection=(Connection)session.getAttribute("DBConnection");
		Properties properties=(Properties)session.getAttribute("properties");
		
		insrtQuery1=properties.getProperty("regist_insertQuery1");
		insrtQuery2=properties.getProperty("regist_insertQuery2");
		      
		try {
			
			
			
			
			PreparedStatement preparedStatement2=connection.prepareStatement(insrtQuery1);
			preparedStatement2.setString(1,registerPojo.getFirstName());
			preparedStatement2.setString(2,registerPojo.getLastName());
			preparedStatement2.setString(3,registerPojo.getMobileNo());
			preparedStatement2.setString(4,registerPojo.getAddress());
			preparedStatement2.setString(5,registerPojo.getEmail());
		    flag=preparedStatement2.execute();
			
			
			PreparedStatement preparedStatement=connection.prepareStatement(insrtQuery2);
		    preparedStatement.setString(1, registerPojo.getEmail());
			preparedStatement.setString(2, registerPojo.getPassword());
			preparedStatement.setString(3, "employee");
			/*registerPojo.getUserType()*/
			flag=preparedStatement.execute();
			
			
		} catch (Exception e) {
			
			logger.error(e);
		}
		
		
		
		
		
		return flag;
	}
}
