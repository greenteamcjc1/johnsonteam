package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.RegisterPojo;

public class Customerprofileupdate {

	
	

	public void profilrUpdate(RegisterPojo registerPojo,HttpServletRequest request)
	{
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("Connection");
		Properties properties=(Properties)session.getAttribute("properties");
		
		String username=(String)session.getAttribute("username");
		System.out.println("=============");
		System.out.println(username);
		String sql=properties.getProperty("customer_profile_update");
		
		try {
			
			PreparedStatement pst=connection.prepareStatement(sql);
			pst.setString(1, registerPojo.getFirstName());
			pst.setString(2, registerPojo.getLastName());
			pst.setString(3, registerPojo.getMobileNo());
			pst.setString(4, username);
			pst.execute();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
