package com.alighthub.tbs.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.util.Mailer;

public class DriverDisplayDao {
	
	static Logger logger=Logger.getLogger("DriverDisplayDao");
	
	public List<DriverPojo1> getDriverData(HttpServletRequest request)
	{
		List<DriverPojo1> list=new ArrayList<DriverPojo1>();

		 
		
		HttpSession session=request.getSession();
		Connection connection=(Connection)session.getAttribute("DBConnection");
		
		String sql="select * from driver";
		Statement statement;
		try {
			
			statement = connection.createStatement();
			ResultSet resultSet=statement.executeQuery(sql);
			int i=0;
			while(resultSet.next())
			{
					DriverPojo1 driverPojo=new DriverPojo1(); 
					
					driverPojo.setFirstName(resultSet.getString(1));
					driverPojo.setLastName(resultSet.getString(2));
					driverPojo.setDOB(resultSet.getString(3));
					driverPojo.setAdharNumber(resultSet.getString(4));	
					driverPojo.setLicenceNumber(resultSet.getString(5));
					driverPojo.setMobileNumber(resultSet.getString(6));
					driverPojo.setVehicle_number(resultSet.getString(7));
				
					list.add(i,driverPojo);
					
					i++;
					
					 
			}
			
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			Mailer.sendEmail();
		}
		
		
		
		
		return list;
	}

}
