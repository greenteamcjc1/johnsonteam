package com.alighthub.tbs.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;

public class AuthenticationFilter implements Filter {
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */

	static Logger logger=Logger.getLogger(AuthenticationFilter.class.getName());
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain)
			throws IOException, ServletException {
		
		
		try{
		
		HttpServletRequest request=(HttpServletRequest)req;
		HttpServletResponse response=(HttpServletResponse)resp;

		
		HttpSession session=request.getSession();
		
		if(session.isNew())
		{ 
			System.out.println("if part--authentication");
			session.invalidate();
			session=request.getSession();
			System.err.println("authentication session id---->>"+session.getId());
	
		}
		
		filterChain.doFilter(request, response);
	    /*response.setHeader("cache-control","no-cache, no-store, must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);*/
		System.err.println("post---->>cahe clear");
		System.err.println("else authentication session id---->>"+session.getId());
		}
		catch(Exception e)
		{
			logger.error("in authentication filter"+e);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
		
		
	}

}
