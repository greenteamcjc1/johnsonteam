package com.alighthub.tbs.filter;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class ConnectionFilter implements Filter {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	String url;
	String driver;
	String dbUser;
	String dbPassword;
	
	static Logger logger=Logger.getLogger(ConnectionFilter.class.getName());
	Connection connection=null;
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain)
			throws IOException, ServletException {
		
		HttpServletRequest request=(HttpServletRequest)req;
		HttpServletResponse response=(HttpServletResponse)resp;
		
		HttpSession session=request.getSession(false);
		
		
		System.out.println("CONNECTION FILTER SESSION ID----->>>"+session.getId());
		
		try {
			
			Properties properties=new Properties();
			InputStream inputStream=ConnectionFilter.class.getClassLoader().getResourceAsStream("/config.properties");
			properties.load(inputStream);
			
			driver=properties.getProperty("driver");
			url=properties.getProperty("url");
			dbUser=properties.getProperty("user");
			dbPassword=properties.getProperty("password");
		
			
			Class.forName(driver);
			
		    connection=DriverManager.getConnection(url,dbUser,dbPassword);
			
		    	session.setAttribute("DBConnection",connection);
		        session.setAttribute("properties",properties);
			
			
			
		} catch (Exception e) {
			
			logger.error(e);
			
		}
      filterChain.doFilter(request, response);
      
      if(connection!=null)
      {
    	  try {
			connection.close();
		} catch (Exception e) {
			logger.error(e);
		}
    	  
      }
		
	
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		
	}

}
