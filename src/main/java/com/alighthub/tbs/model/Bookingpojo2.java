package com.alighthub.tbs.model;

import java.sql.Date;

public class Bookingpojo2 {

	private int bookingID;
	
	public int getBookingID() {
		return bookingID;
	}
	public void setBookingID(int bookingID) {
		this.bookingID = bookingID;
	}
	private float amount;
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getVehicleno() {
		return Vehicleno;
	}
	public void setVehicleno(String vehicleno) {
		Vehicleno = vehicleno;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getPlaceform() {
		return placeform;
	}
	public void setPlaceform(String placeform) {
		this.placeform = placeform;
	}
	public String getPlaceto() {
		return placeto;
	}
	public void setPlaceto(String placeto) {
		this.placeto = placeto;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getNo_of_vehicle() {
		return No_of_vehicle;
	}
	public void setNo_of_vehicle(String no_of_vehicle) {
		No_of_vehicle = no_of_vehicle;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	private String delivered_date;
	public String getDelivered_date() {
		return delivered_date;
	}
	public void setDelivered_date(String delivered_date) {
		this.delivered_date = delivered_date;
	}
	private String firstname;
	private String Vehicleno;
	private String lastname;
	private String mobile_no;
	private String email;
	private String date;
	private String time;
	private String placeform;
	private String placeto;
	private String weight;
	private String No_of_vehicle;
	private String address;
	private String deliveredaddress;
	private int counter;
	public String getDeliveredaddress() {
		return deliveredaddress;
	}
	public void setDeliveredaddress(String deliveredaddress) {
		this.deliveredaddress = deliveredaddress;
	}
	
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
}
