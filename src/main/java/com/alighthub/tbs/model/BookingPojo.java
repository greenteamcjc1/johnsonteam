package com.alighthub.tbs.model;

public class BookingPojo {
   
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	private String Messages;
	
	public String getMessages() {
		return Messages;
	}
	public void setMessages(String messages) {
		Messages = messages;
	}
	private int Booking_Id;
	private String First_Name;
	private String Last_Name;
	private String Mobile_Number;
	private String Email;
	private String Address;
	private String Booking_Date;
	private String Required_Date;
	private String Required_Time;
	private String Source;
	private String Destination;
	private String Vehicle_Number;
	private String Fare;
	private String Weight;
	private String NumberOf_Vehicle;
	private String Delivered_Address;
	private int TotalDistance;
	public int getTotalDistance() {
		return TotalDistance;
	}
	public void setTotalDistance(int totalDistance) {
		TotalDistance = totalDistance;
	}
	private int Count;
	
	public String getFirst_Name() {
		return First_Name;
	}
	public void setFirst_Name(String first_Name) {
		First_Name = first_Name;
	}
	public String getLast_Name() {
		return Last_Name;
	}
	public void setLast_Name(String last_Name) {
		Last_Name = last_Name;
	}
	public String getMobile_Number() {
		return Mobile_Number;
	}
	public void setMobile_Number(String mobile_Number) {
		Mobile_Number = mobile_Number;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getBooking_Date() {
		return Booking_Date;
	}
	public void setBooking_Date(String booking_Date) {
		Booking_Date = booking_Date;
	}
	public String getRequired_Date() {
		return Required_Date;
	}
	public void setRequired_Date(String required_Date) {
		Required_Date = required_Date;
	}
	public String getRequired_Time() {
		return Required_Time;
	}
	public void setRequired_Time(String required_Time) {
		Required_Time = required_Time;
	}
	public String getSource() {
		return Source;
	}
	public void setSource(String source) {
		Source = source;
	}
	public String getDestination() {
		return Destination;
	}
	public void setDestination(String destination) {
		Destination = destination;
	}
	public String getVehicle_Number() {
		return Vehicle_Number;
	}
	public void setVehicle_Number(String vehicle_Number) {
		Vehicle_Number = vehicle_Number;
	}
	public String getFare() {
		return Fare;
	}
	public void setFare(String fare) {
		Fare = fare;
	}
	public String getWeight() {
		return Weight;
	}
	public void setWeight(String weight) {
		Weight = weight;
	}
	public String getNumberOf_Vehicle() {
		return NumberOf_Vehicle;
	}
	public void setNumberOf_Vehicle(String numberOf_Vehicle) {
		NumberOf_Vehicle = numberOf_Vehicle;
	}
	public String getDelivered_Address() {
		return Delivered_Address;
	}
	public void setDelivered_Address(String delivered_Address) {
		Delivered_Address = delivered_Address;
	}
	public int getBooking_Id() {
		return Booking_Id;
	}
	public void setBooking_Id(int booking_Id) {
		Booking_Id = booking_Id;
	}
	public int getCount() {
		return Count;
	}
	public void setCount(int count) {
		Count = count;
	}
	
	
	
}
