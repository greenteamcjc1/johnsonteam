package com.alighthub.tbs.model;

public class ReportPojo {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	private String Messages;
	private String FromDate;
	private String ToDate;
	private String Document;
	public String getMessages() {
		return Messages;
	}
	public void setMessages(String messages) {
		Messages = messages;
	}
	public String getFromDate() {
		return FromDate;
	}
	public void setFromDate(String fromDate) {
		FromDate = fromDate;
	}
	public String getToDate() {
		return ToDate;
	}
	public void setToDate(String toDate) {
		ToDate = toDate;
	}
	public String getDocument() {
		return Document;
	}
	public void setDocument(String document) {
		Document = document;
	}
	
	
}
