package com.alighthub.tbs.model;

public class VehiclePojo {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	private String VEHICLE_NUMBER;
	private String MODEL_NUMBER;
	private String VEHICLE_TYPE;
	private String OWNER;
	private String INSURANCE_NUMBER;
	private String PERMIT_NUMBER;
	private int AVERAGE_SPEED;
	private int CAPACITY;
	public String getVEHICLE_NUMBER() {
		return VEHICLE_NUMBER;
	}
	public void setVEHICLE_NUMBER(String vEHICLE_NUMBER) {
		VEHICLE_NUMBER = vEHICLE_NUMBER;
	}
	public String getMODEL_NUMBER() {
		return MODEL_NUMBER;
	}
	public void setMODEL_NUMBER(String mODEL_NUMBER) {
		MODEL_NUMBER = mODEL_NUMBER;
	}
	public String getVEHICLE_TYPE() {
		return VEHICLE_TYPE;
	}
	public void setVEHICLE_TYPE(String vEHICLE_TYPE) {
		VEHICLE_TYPE = vEHICLE_TYPE;
	}
	public String getOWNER() {
		return OWNER;
	}
	public void setOWNER(String oWNER) {
		OWNER = oWNER;
	}
	public String getINSURANCE_NUMBER() {
		return INSURANCE_NUMBER;
	}
	public void setINSURANCE_NUMBER(String iNSURANCE_NUMBER) {
		INSURANCE_NUMBER = iNSURANCE_NUMBER;
	}
	public String getPERMIT_NUMBER() {
		return PERMIT_NUMBER;
	}
	public void setPERMIT_NUMBER(String pERMIT_NUMBER) {
		PERMIT_NUMBER = pERMIT_NUMBER;
	}
	public int getAVERAGE_SPEED() {
		return AVERAGE_SPEED;
	}
	public void setAVERAGE_SPEED(int aVERAGE_SPEED) {
		AVERAGE_SPEED = aVERAGE_SPEED;
	}
	public int getCAPACITY() {
		return CAPACITY;
	}
	public void setCAPACITY(int cAPACITY) {
		CAPACITY = cAPACITY;
	}
}
