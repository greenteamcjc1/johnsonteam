package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.DriverDeleteDao;
 import com.alighthub.tbs.model.DriverPojo1;
 import com.alighthub.tbs.service.DriverDeleteService;
 
public class DriverDeleteServiceImplementation  implements DriverDeleteService{

	@Override
	public int deleteDriverRecord(DriverPojo1 driverPojo ,HttpServletRequest request) {
		 
		DriverDeleteDao dao=new DriverDeleteDao();
		int x=dao.deleteDriverRecord(driverPojo, request);
			
		return x;
	}

}
