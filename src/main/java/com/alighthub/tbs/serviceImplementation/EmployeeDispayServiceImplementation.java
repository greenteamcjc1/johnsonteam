package com.alighthub.tbs.serviceImplementation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.EmployeeDispayDao;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.service.EmployeeDisplayService;

public class EmployeeDispayServiceImplementation implements EmployeeDisplayService {

	
	public List<RegisterPojo> getEmployeeData(HttpServletRequest request)
	{
		EmployeeDispayDao employeeDispayDao=new EmployeeDispayDao();
		List<RegisterPojo > list=employeeDispayDao.getEmployeeData(request);
		
		return list;
	}
}
