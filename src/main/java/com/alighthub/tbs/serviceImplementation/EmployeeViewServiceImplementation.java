package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.EmployeeViewDao;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.service.EmployeeViewService;

public class EmployeeViewServiceImplementation implements EmployeeViewService {

	@Override
	public RegisterPojo upadateEmployeeRecord(RegisterPojo registerPojo,
			HttpServletRequest request) {
		
		EmployeeViewDao dao=new EmployeeViewDao();
		RegisterPojo pojo=dao.upadateEmployeeRecord(registerPojo, request);
		
		
		return pojo;
		 
	}
	
	

}
