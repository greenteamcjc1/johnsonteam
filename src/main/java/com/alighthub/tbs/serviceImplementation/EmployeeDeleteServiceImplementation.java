package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.EmployeeDeleteDao;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.service.EmployeeDeleteService;

public class EmployeeDeleteServiceImplementation  implements EmployeeDeleteService{

	@Override
	public int deleteEmployeerecord(RegisterPojo registerPojo,HttpServletRequest request) {
		 
		EmployeeDeleteDao dao=new EmployeeDeleteDao();
		int x=dao.deleteEmployeerecord(registerPojo, request);
			
		return x;
	}

}
