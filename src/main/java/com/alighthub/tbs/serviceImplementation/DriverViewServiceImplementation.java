package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.DriverViewDao;
import com.alighthub.tbs.dao.EmployeeViewDao;
import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.service.DriverViewService;
import com.alighthub.tbs.service.EmployeeViewService;

public class DriverViewServiceImplementation implements DriverViewService {

	@Override
	public DriverPojo1 upadateDriverRecord(DriverPojo1 driverPojo,HttpServletRequest request) {
		
		DriverViewDao dao=new DriverViewDao();
		DriverPojo1 pojo=dao.upadateDriverRecord(driverPojo, request);
		
		
		return pojo;
		 
	}
	
	

}
