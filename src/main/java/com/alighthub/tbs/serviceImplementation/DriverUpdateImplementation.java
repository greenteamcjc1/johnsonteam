package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.DriverUpdateDao;
import com.alighthub.tbs.dao.EmployeeUpdateDao;
import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.service.DriverUpdateService;
import com.alighthub.tbs.service.EmployeeUpdateService;

public class DriverUpdateImplementation  implements DriverUpdateService{

	@Override
	public int updateDriverRecord(DriverPojo1 driverPojo,	HttpServletRequest request) {
	 	
		DriverUpdateDao dao=new DriverUpdateDao();
		int x=dao.updateDriverRecord(driverPojo, request);
		
		return x;
		
	}

}
