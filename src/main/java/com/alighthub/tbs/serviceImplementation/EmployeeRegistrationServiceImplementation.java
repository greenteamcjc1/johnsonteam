package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.EmployeeRegistrationDao;
import com.alighthub.tbs.dao.HomeRegisterDao;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.service.EmployeeRegistrationService;

public class EmployeeRegistrationServiceImplementation implements EmployeeRegistrationService {

	@Override
	public boolean registerEmployee(RegisterPojo registerPojo,	HttpServletRequest request) {
		  
		EmployeeRegistrationDao dao=new EmployeeRegistrationDao();
		boolean b=dao.registerEmployee(registerPojo,request);
		
		return b;
	}
	
	
	

}
