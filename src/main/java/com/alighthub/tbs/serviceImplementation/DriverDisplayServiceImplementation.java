package com.alighthub.tbs.serviceImplementation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.DriverDisplayDao;
import com.alighthub.tbs.dao.EmployeeDispayDao;
import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.service.DriverDisplayService;
import com.alighthub.tbs.service.EmployeeDisplayService;

public class DriverDisplayServiceImplementation implements DriverDisplayService{

	
	public List<DriverPojo1> getDriverData(HttpServletRequest request)
	{
		DriverDisplayDao driverDispayDao=new DriverDisplayDao();
		List<DriverPojo1 > list=driverDispayDao.getDriverData(request);
		
		return list;
	}
}
