package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.EmployeeUpdateDao;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.service.EmployeeUpdateService;

public class EmployeeUpdateImplementation  implements EmployeeUpdateService{

	@Override
	public int updateEmployeeRecord(RegisterPojo registerPojo,	HttpServletRequest request) {
	 	
		EmployeeUpdateDao dao=new EmployeeUpdateDao();
		int x=dao.updateEmployeeRecord(registerPojo, request);
		
		return x;
		
	}

}
