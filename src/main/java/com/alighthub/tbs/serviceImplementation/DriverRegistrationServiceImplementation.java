package com.alighthub.tbs.serviceImplementation;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.dao.DriverRegistrationDao;
import com.alighthub.tbs.dao.EmployeeRegistrationDao;
import com.alighthub.tbs.dao.HomeRegisterDao;
import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;
import com.alighthub.tbs.service.DriverRegistrationService;
import com.alighthub.tbs.service.EmployeeRegistrationService;

public class DriverRegistrationServiceImplementation implements DriverRegistrationService {

	@Override
	public boolean registerDriver(DriverPojo1 driverPojo, HttpServletRequest request) {
		  
		DriverRegistrationDao dao=new DriverRegistrationDao();
		boolean b=dao.registerDriver(driverPojo,request);
		
		return b;
	}
	
	
	

}
