package com.alighthub.tbs.serviceImplementation;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import com.alighthub.tbs.dao.HomeLoginDao;
import com.alighthub.tbs.dao.HomeRegisterDao;
import com.alighthub.tbs.model.LoginPojo;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.service.HomeService;


/**
 * 
 * @author Ajit
 *  Date:-22/Aug/2017
 *  Discription:-Home Page ServicImplementation
 *
 */
public class HomeServiceImplementation implements HomeService {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	static Logger logger=Logger.getLogger(HomeServiceImplementation.class.getName());

	boolean flag;
	
	/**
	 * Home Registration DataInsert
	 */
	
	
	@Override
	public boolean registerData_Insert(RegisterPojo registerPojo,HttpServletRequest request) {
		
		try
		{
		HomeRegisterDao homeRegisterDao=new HomeRegisterDao();
		 flag=homeRegisterDao.registerData_Insert(registerPojo,request);
		return flag;
		}
		catch(Exception e)
		{
			logger.error(e);
			
		}
		return flag;
	}
	
	/**
	 * Home Login Check
	 */

	@Override
	public LoginPojo loginCheck(LoginPojo loginPojo, HttpServletRequest request) {
		
		HomeLoginDao homeLoginDao=new HomeLoginDao();
		loginPojo=homeLoginDao.loginCheck(loginPojo, request);
		
		return loginPojo;
	}

	
	
	@Override
	public RegisterPojo getEmployeeDetails(RegisterPojo registerPojo,HttpServletRequest request) {
		
		HomeLoginDao homeLoginDao=new HomeLoginDao();
		registerPojo=homeLoginDao.getEmployeeDetails(registerPojo, request);
		
		return registerPojo;
	}

}
