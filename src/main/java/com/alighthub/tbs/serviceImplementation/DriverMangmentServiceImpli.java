package com.alighthub.tbs.serviceImplementation;

import java.util.ArrayList;
import java.util.List;

import com.alighthub.tbs.dao.DriverDao;
import com.alighthub.tbs.model.DriverPojo;
import com.alighthub.tbs.service.DriverMangmentService;


public class DriverMangmentServiceImpli implements DriverMangmentService {

	List<DriverPojo>Driver=new ArrayList<DriverPojo>();

	

	
	
	DriverDao driverdao=new  DriverDao();
	

	@Override
	public void createDriver(DriverPojo driverPojo) {
		
		driverdao.createDriver(driverPojo);

	}

	@Override
	public void updateDriver(DriverPojo driverPojo) {
		driverdao.updateDriver(driverPojo);
	}

	@Override
	public List<DriverPojo> AllDriver() {
		
		Driver=driverdao.AllDriver();
		return Driver;
	}

	@Override
	public void deleteDriver(String firstname) {
		driverdao.deleteDriver(firstname);
		
	}


}
