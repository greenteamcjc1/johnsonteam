package com.alighthub.tbs.serviceImplementation;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.alighthub.tbs.dao.EmpRegisteredGetDataBookingDao;
import com.alighthub.tbs.dao.EmployeeBookingDao;
import com.alighthub.tbs.dao.EmployeeRegisteredBookingDao;
import com.alighthub.tbs.dao.EmployeeSearchBoxDao;
import com.alighthub.tbs.dao.NotificationDao;
import com.alighthub.tbs.dao.OrderActivationDao;
import com.alighthub.tbs.dao.ReportGenerateDao;
import com.alighthub.tbs.dao.AllRecordDao;
import com.alighthub.tbs.dao.AmountCalculationDao;
import com.alighthub.tbs.designpattern.EmployeeLogOut;
import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.model.ReportPojo;
import com.alighthub.tbs.model.VehiclePojo;
import com.alighthub.tbs.service.EmployeeService;

public class EmployeeServiceImplementation implements EmployeeService{

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	
	
	@Override
	public HttpSession employeeLogOut(HttpSession session) {
	   
		System.err.println("SERVICE IMPLEMENTATION SESSION ID------>>>"+session.getId());
		EmployeeLogOut employeeLogOut=new EmployeeLogOut();
		employeeLogOut.employeeLogOut(session);
		return session;
	}

	
	
	/**
	 * 
	 *   insert  New booking Unregistered data
	 * 
	 */
	@Override
	public boolean employeeBooking(BookingPojo bookingPojo, HttpServletRequest request) {
		
		
		
		EmployeeBookingDao employeeBookingDao=new EmployeeBookingDao();
		boolean report=employeeBookingDao.employeeBooking(bookingPojo, request);
		
		return report;
	}
	
	/**
	 * Get data from registered booking table
	 */
	
	@Override
	public ArrayList<Object> registeredgetdataBooking(HttpServletRequest request, RegisterPojo registerPojo) {
		
		    EmpRegisteredGetDataBookingDao dataBookingDao=new EmpRegisteredGetDataBookingDao();
		    ArrayList<Object>arrayList =dataBookingDao.registeredgetdataBooking(request, registerPojo);
		
		
		return arrayList;
	}
	
	/**
	 * Insert registered booking data
	 */
	
	
	@Override
	public boolean registeredBooking(BookingPojo bookingPojo, HttpServletRequest request) {
		
		
		          EmployeeRegisteredBookingDao employeeRegisteredBookingDao=new EmployeeRegisteredBookingDao();
		          boolean report=employeeRegisteredBookingDao.registeredBooking(bookingPojo, request);
		
				return report;
	}
	
	
	/**
	 * get registered data and unregistered data
	 * 
	 */
	
	
	@Override
	public ArrayList<BookingPojo> allRecord(BookingPojo bookingPojo, HttpServletRequest request) {
		
		AllRecordDao recordDao=new AllRecordDao();
		ArrayList<BookingPojo>arrayList=recordDao.allRecord(bookingPojo, request);
		
		return arrayList;
	}
	
	
	
	/**
	 * 
	 * Multiple Search box--email search
	 */
	
	@Override
	public ArrayList<Object> searchEmailDetails(HttpServletRequest request, BookingPojo bookingPojo) {
		System.out.println("Service implementation");
		EmployeeSearchBoxDao searchBoxDao=new EmployeeSearchBoxDao();
		ArrayList<Object>arrayList=searchBoxDao.searchEmailDetails(request, bookingPojo);
		
		
		
		return arrayList;
	}
	
	/**
	 * 
	 * Multiple Search box--vehicle details
	 */
	
	@Override
	public ArrayList<Object> searchVehicleDetails(HttpServletRequest request, VehiclePojo vehiclePojo) {
             
		        EmployeeSearchBoxDao searchBoxDao=new EmployeeSearchBoxDao();
		        ArrayList<Object>arrayList=searchBoxDao.searchVehicleDetails(request, vehiclePojo);

		
		return arrayList;
	}
	
	/**
	 * 
	 * Multiple Search box--booking id Details
	 */
	
	@Override
	public ArrayList<Object> searchBookingIdDetails(HttpServletRequest request, BookingPojo bookingPojo) {
		
		EmployeeSearchBoxDao searchBoxDao=new EmployeeSearchBoxDao();
		ArrayList<Object>arrayList=searchBoxDao.searchBookingIdDetails(request, bookingPojo);
		
		
		return arrayList;
	}
	
	
	/**
	 * Notification System
	 */
	
	
	@Override
	public ArrayList<Object> Notification(HttpServletRequest request, BookingPojo bookingPojo) {

                NotificationDao notificationDao=new NotificationDao();
                ArrayList<Object>arrayList=notificationDao.Notification(request, bookingPojo);
		
		
		return arrayList;
	}
	
	/**
	 * 
	 * Fare Calculation 
	 */
	
	@Override
	public BookingPojo AmountCalculation(HttpServletRequest request, BookingPojo bookingPojo) {
		
		AmountCalculationDao calculationDao=new AmountCalculationDao();
		BookingPojo bookingPojo2=calculationDao.AmountCalculation(request, bookingPojo);
		
		return bookingPojo2;
	}
	
	/**
	 * update fare and vehicle number
	 */
	
	@Override
	public ArrayList<Object> orderActivation(ArrayList<Object> arrayList, BookingPojo bookingPojo,
			HttpServletRequest request) {
		
		OrderActivationDao orderActivationDao=new OrderActivationDao();
		arrayList=orderActivationDao.orderActivation(arrayList, bookingPojo, request);
		
		
		return arrayList;
	}
	
	
	/**
	 * genereate customer booking section report
	 */
	
	
	@Override
	public ArrayList<Object> generateReport(HttpServletRequest request, ReportPojo reportPojo) {

      ReportGenerateDao generateDao=new ReportGenerateDao();
      ArrayList<Object>arrayList=generateDao.generateReport(request, reportPojo);
		
		
		return arrayList;
	}
	
}
