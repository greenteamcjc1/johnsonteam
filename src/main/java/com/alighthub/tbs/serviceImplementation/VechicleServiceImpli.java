package com.alighthub.tbs.serviceImplementation;

import java.util.ArrayList;
import java.util.List;

import com.alighthub.tbs.dao.VechileDao;
import com.alighthub.tbs.model.VechiclePojo;
import com.alighthub.tbs.service.VechicleService;



public class VechicleServiceImpli implements VechicleService {

	List<VechiclePojo>vechicle=new ArrayList<VechiclePojo>();
	VechileDao dao=new VechileDao();

	public List<VechiclePojo> AllVechicle() {
	
		vechicle=dao.AllVechicle();
		return vechicle;
	}
	
	

	public void createVechicle(VechiclePojo vechiclePojo) {
		// TODO Auto-generated method stub
		
		dao.createVechicle(vechiclePojo);
	}
	
	

	public void deleteVechicle(String number) {
		// TODO Auto-generated method stub
		
		dao.deleteVechicle(number);
	}
	
	
	
	public void updateVechicle(VechiclePojo vechiclePojo) {
		dao.updateVechicle(vechiclePojo);
		
	}
	
}
