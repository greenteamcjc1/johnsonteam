package com.alighthub.tbs.util;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ContextListenerLog4j implements ServletContextListener {

	
	static Logger logger = Logger.getLogger("ContextListenerLog4j");
	
	@Override
	public void contextDestroyed(ServletContextEvent event) {
	
		
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		try
		{
		ServletContext context = event.getServletContext();
        String log4jConfigFile = context.getInitParameter("log4j.properties");
        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
       /*logger.info(System.getProperty("java.class.path"));
       logger.info(fullPath);*/
       System.out.println("listyufg");
        PropertyConfigurator.configure(fullPath);
		}
		catch (Exception e) {
			
			logger.error(e);
		}
	}

	
	
	
	
	
	
}
