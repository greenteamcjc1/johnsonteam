package com.alighthub.tbs.service;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;

public interface DriverUpdateService {
	
	public int updateDriverRecord(DriverPojo1 driverPojo , HttpServletRequest request);

}
