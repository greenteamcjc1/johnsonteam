package com.alighthub.tbs.service;
import javax.servlet.http.HttpServletRequest;
import com.alighthub.tbs.model.LoginPojo;
import com.alighthub.tbs.model.RegisterPojo;

public interface HomeService {
	
	
	
	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */

	public boolean registerData_Insert(RegisterPojo registerPojo,HttpServletRequest request);
	
	
	public LoginPojo loginCheck(LoginPojo loginPojo,HttpServletRequest request);
	
	public RegisterPojo getEmployeeDetails(RegisterPojo registerPojo,HttpServletRequest request);
	
}
