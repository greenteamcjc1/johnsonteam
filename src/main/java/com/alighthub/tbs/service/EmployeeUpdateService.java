package com.alighthub.tbs.service;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.RegisterPojo;

public interface EmployeeUpdateService {
	
	public int updateEmployeeRecord(RegisterPojo registerPojo , HttpServletRequest request);

}
