package com.alighthub.tbs.service;

 
import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.DriverPojo1;
 
public interface DriverDeleteService {
	
	public int deleteDriverRecord(DriverPojo1 driverPojo ,HttpServletRequest request );

}
