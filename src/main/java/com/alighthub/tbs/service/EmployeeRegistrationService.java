package com.alighthub.tbs.service;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.RegisterPojo;

public interface EmployeeRegistrationService {
	
	
	public boolean registerEmployee(RegisterPojo registerPojo, HttpServletRequest request);

}
