package com.alighthub.tbs.service;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;

public interface DriverRegistrationService {
	
	
	public boolean registerDriver(DriverPojo1  driverPojo, HttpServletRequest request);

}
