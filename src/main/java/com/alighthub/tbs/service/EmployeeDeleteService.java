package com.alighthub.tbs.service;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.RegisterPojo;

public interface EmployeeDeleteService {
	
	public int deleteEmployeerecord(RegisterPojo registerPojo,HttpServletRequest request );

}
