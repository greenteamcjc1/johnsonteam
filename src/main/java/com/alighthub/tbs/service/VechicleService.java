package com.alighthub.tbs.service;

import java.util.List;

import com.alighthub.tbs.model.VechiclePojo;



public interface VechicleService {

	
	abstract public void deleteVechicle(String number);
	abstract public void createVechicle(VechiclePojo vechiclePojo);
	abstract public void updateVechicle(VechiclePojo vechiclePojo);
	abstract public List<VechiclePojo> AllVechicle();
}
