package com.alighthub.tbs.service;

import java.util.ArrayList;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.alighthub.tbs.model.BookingPojo;
import com.alighthub.tbs.model.RegisterPojo;
import com.alighthub.tbs.model.ReportPojo;
import com.alighthub.tbs.model.VehiclePojo;

public interface EmployeeService {

	/**
	 * 
	 * @author Ajit
	 * Modul:-Employee
	 */
	
	public HttpSession employeeLogOut(HttpSession session);
	
	public boolean employeeBooking(BookingPojo bookingPojo,HttpServletRequest request);
	
	public ArrayList<Object> registeredgetdataBooking(HttpServletRequest request,RegisterPojo registerPojo);
	
	public boolean registeredBooking(BookingPojo bookingPojo,HttpServletRequest request);
	
	public ArrayList<BookingPojo> allRecord(BookingPojo bookingPojo,HttpServletRequest request);
	
	public ArrayList<Object> searchEmailDetails(HttpServletRequest request,BookingPojo bookingPojo);
	
	public ArrayList<Object> searchVehicleDetails(HttpServletRequest request,VehiclePojo vehiclePojo);
	
	public ArrayList<Object> searchBookingIdDetails(HttpServletRequest request,BookingPojo bookingPojo);
	
	public ArrayList<Object> Notification(HttpServletRequest request,BookingPojo bookingPojo);
	
	public BookingPojo AmountCalculation(HttpServletRequest request,BookingPojo bookingPojo);
	
	public ArrayList<Object> orderActivation(ArrayList<Object> arrayList,BookingPojo bookingPojo,HttpServletRequest request);
	
	public ArrayList<Object> generateReport(HttpServletRequest request,ReportPojo reportPojo);
}
