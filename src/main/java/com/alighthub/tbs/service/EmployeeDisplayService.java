package com.alighthub.tbs.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.RegisterPojo;

public interface EmployeeDisplayService {

	public List<RegisterPojo> getEmployeeData(HttpServletRequest request); 
	
}
