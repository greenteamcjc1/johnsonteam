package com.alighthub.tbs.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.alighthub.tbs.model.DriverPojo1;
import com.alighthub.tbs.model.RegisterPojo1;

public interface DriverDisplayService {

	public List<DriverPojo1> getDriverData(HttpServletRequest request); 
	
}
