package com.alighthub.tbs.service;

import java.util.List;

import com.alighthub.tbs.model.DriverPojo;


public interface DriverMangmentService {


	abstract public void deleteDriver(String firstname);
	abstract public void createDriver(DriverPojo driverPojo);
	abstract public void updateDriver(DriverPojo driverPojo);
	abstract public List<DriverPojo> AllDriver();
	
}
