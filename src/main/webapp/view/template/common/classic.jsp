<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  
  	   <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
       <!--  Font-awesome -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Fonts Form Google Web Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Lato:400,300,400italic,700,900,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto:500' rel='stylesheet' type='text/css'>
  
        <!--  Responsive Mobile Menu -->
        <link rel="stylesheet" href="/webapp/css/slicknav.css" />
        <!-- Revolation Slider -->
        <link href="/webapp/rs-plugin/css/settings.css" rel="stylesheet">
        <!-- Carousel Slider -->
        <link href="/webapp/css/owl.carousel.css" rel="stylesheet">
        <!-- Main Style Css -->
        <link href="style.css" rel="stylesheet">
        <!-- Responsive -->
        <link href="/webapp/css/responsive.css" rel="stylesheet">
        
        
        
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

		<!--jQuery-->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <!-- Carousel Slider -->
        <script src="/webapp/js/owl.carousel.min.js"></script>
        <!-- Revolation Slider -->
        <script src="/webapp/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script src="/webapp/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>		
        <!-- Mobile Menu Js -->
        <script src="/webapp/js/jquery.slicknav.min.js"></script>
        <!-- Active jQuery -->
        <script src="/webapp/js/main.js"></script>
		
		 <!--     Fonts     -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
		 <link href="/css/bootstrap.css" rel="stylesheet" />
	     <link href="/css/coming-sssoon.css" rel="stylesheet" />   
	     
	     
	     <!-- Body Pages -->
			
	<!-- Bootstrap Styles-->
   <!-- <link href="assets/css/bootstrap.css" rel="stylesheet" />
     <!-- FontAwesome Styles-->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
	
    <link href="assets/css/select2.min.css" rel="stylesheet" >
	<link href="assets/css/checkbox3.min.css" rel="stylesheet" >
        <!-- Custom Styles-->
     <!-- Google Fonts-->
   <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
	 
</head>
<body>
	<div class="table-responsive">
		<table class="table">

			<tr>
				<td><tiles:insertAttribute name="header"></tiles:insertAttribute></td>
			</tr>

			<tr>
				<td><tiles:insertAttribute name="menu"></tiles:insertAttribute></td>
			</tr>
			<tr>
				<td><tiles:insertAttribute name="body"></tiles:insertAttribute></td>
			</tr>
			<tr>
				<td
					style="background-color: #bac4c4; border: 2px solid; border-style: double;"><tiles:insertAttribute
						name="footer"></tiles:insertAttribute></td>
			</tr>

		</table>
	</div>
</body>
</html>