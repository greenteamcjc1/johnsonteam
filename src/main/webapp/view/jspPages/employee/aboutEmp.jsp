<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>

         <%response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0); %>
        <section class="promotions_area section_padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-lg-10 col-md-offset-1 col-lg-offset-1">
                        <div class="page_title text-center">
                           
                            <p>Never heard the word impossible. This time there's no stopping us. These Happy Days are yours and mine Happy Days. Today still wanted  they survive as soldiers of fortune. Said Californ'y is loaded up the truck and moved.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="single_promotions text-center">
                            <div class="s_promotion_icon">
                                <img src="img/track_icon.png" alt="">
                            </div>
                            <h2>we make it faster</h2>
                            <p>They really are a scream the Addams Family. It's time to put on makeup. It's time to dress up right. It's time to raise the curtain  </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="single_promotions text-center">
                            <div class="s_promotion_icon">
                                <img src="img/hand_icon.png" alt="">
                            </div>
                            <h2>save and secure move</h2>
                            <p>They really are a scream the Addams Family. It's time to put on makeup. It's time to dress up right. It's time to raise the curtain  </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="single_promotions text-center">
                            <div class="s_promotion_icon">
                                <img src="img/alarm_clock_icon.png" alt="">
                            </div>
                            <h2>on time delevery</h2>
                            <p>They really are a scream the Addams Family. It's time to put on makeup. It's time to dress up right. It's time to raise the curtain  </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Promotions Area-->
        <!--Our Great Team Area -->
        <section class="process_area section_padding section_gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="page_title text-center">
                            <h2>Our Great Team</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-6">
                        <div class="single_team">
                            <a href=""><img src="img/team_1.jpg" alt=""></a>
                            <div class="team_discription">
                                <h3>michel wague</h3>
                                <p>ceo/founder</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6">
                        <div class="single_team">
                            <a href=""><img src="img/team_2.jpg" alt=""></a>
                            <div class="team_discription">
                                <h3>George hilson</h3>
                                <p>driver</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6">
                        <div class="single_team">
                            <a href=""><img src="img/team_3.jpg" alt=""></a>
                            <div class="team_discription">
                                <h3>chries hemsworth</h3>
                                <p>delevery boy</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-6">
                        <div class="single_team">
                            <a href=""><img src="img/team_4.jpg" alt=""></a>
                            <div class="team_discription">
                                <h3>chris evans</h3>
                                <p>manager</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Our Great Team Area-->
        <!--Start Work Area  -->
        <section class="work_area section_padding">
            <div class="container">
               <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="page_title">
                            <h2>reasons to choose us</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="single_work">
                            <a href=""><img class="alignleft" src="img/24_7.png" alt=""></a>
                            <h4>24/4 Service</h4>
                            <p>Three's company too. Michael Knight a young loner on a crusade to the innocent</p>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="alignleft" src="img/track_two.png" alt=""></a>
                            <h4>over 750 vehicles</h4>
                            <p>Three's company too. Michael Knight a young loner on a crusade to the innocent</p>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="alignleft" src="img/man_icon.png" alt=""></a>
                            <h4>sequrity cleared drivers</h4>
                            <p>Three's company too. Michael Knight a young loner on a crusade to the innocent</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="single_work">
                            <a href=""><img class="alignleft" src="img/map_maker.png" alt=""></a>
                            <h4>live tracking</h4>
                            <p>Three's company too. Michael Knight a young loner on a crusade to the innocent</p>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="alignleft" src="img/sms_mail.png" alt=""></a>
                            <h4>sms / emails alerts</h4>
                            <p>Three's company too. Michael Knight a young loner on a crusade to the innocent</p>
                        </div>
                        <div class="single_work">
                            <a href=""><img class="alignleft" src="img/fire_clock.png" alt=""></a>
                            <h4>estamited delevery time</h4>
                            <p>Three's company too. Michael Knight a young loner on a crusade to the innocent</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="work_area_img">
                            <img src="img/work_man_two.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Work Area-->
        <!--Start Contact Now Area -->
        <section class="contact_now_area_about_page">
            <div class="contact_now_area_overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-md-offset-6 col-lg-offset-6">
                        <div class="contact_now_text">
                            <div class="contact_now_display_table_cell">
                                <h3>Our commitment to quality and security means that we only trained staff for packing and removal services. </h3>
                                <a href="" class="contact_button">CONTACT NOW</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
        <!--End Contact Now Area-->
        <!-- Slider Bottom Area-->
        <section class="slider_bottom_area section_padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="slider_bottom">
                            <div class="single_slide">
                            <a href="#"><img src="img/slide_1.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_2.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_3.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_4.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_5.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_6.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_1.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_2.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_3.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_4.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_5.jpg" alt=""></a>
                            <a href="#"><img src="img/slide_6.jpg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Slider Bottom Area-->
        <!-- Newsletter Area -->
        <section class="newsletter_area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                       <div class="signup_newsletter">
                           <p>singup for our</p>
                           <h3>newsletter</h3>
                           <form action="process.php">
                               <input type="email" placeholder="e-mail address" required>
                               <input type="submit" value="suscribe">
                           </form>
                       </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Newsletter Area-->
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="footer_border"></div>
                        </div>
                    </div>
                    
                        <div class="col-md-6 col-lg-6">
                            <div class="footer_list">
                                <h3>our service</h3>
                                <div class="col-md-6 col-lg-6">
                                    <ul>
                                        <li><a href="#">Moving locally or Interstate</a></li>
                                        <li><a href="#">Moving Oversead</a></li>
                                        <li><a href="#">Commercial Relocation</a></li>
                                        <li><a href="#">Corporate Relocation</a></li>
                                        <li><a href="#">Packing</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-6 col-lg-6">
                                    <ul>
                                        <li><a href="#">Storage</a></li>
                                        <li><a href="#">Insurance</a></li>
                                        <li><a href="#">Additional Service</a></li>
                                        <li><a href="#">Contuct Us</a></li>
                                        <li><a href="#">Quality Policy Statement</a></li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="footer_bottom_area">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="footer_copyright">
                                <p>Copyright 2016 &copy; <a href ="http://freecssthemes.com/">FreeCssThemes</a> | All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--End Footer Area-->
        
        






</body>
</html>