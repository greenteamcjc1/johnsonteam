<!DOCTYPE>
<html>
<head>
<title>CRUD operations using jTable in J2EE</title>
<!-- Include one of jTable styles. -->
<!-- <link href="css/metro/blue/jtable.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
Include jTable script file.
<script src="js/jquery-1.8.2.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
<script src="js/jquery.jtable.js" type="text/javascript"></script>
 -->
<script type="text/javascript">
	$(document).ready(function() {
		$('#VechicleTableContainer').jtable({
			title : 'Vechicle Management',
			actions : {
				listAction : 'VController?action=list',
				createAction : 'VController?action=create',
				updateAction : 'VController?action=update',
				deleteAction : 'VController?action=delete'
			},
			
			fields : {
				Number :{ 
					title :'Number',
					width :'10%',
					key :true,
					list :true,
					edit :false,
					create :true
				},
				
				modelNo : {
					title : 'ModelNo',
					width : '10%',
					edit : true
				},
				
				insurenceNo : {
					title : 'InsurenceNo',
					width : '10%',
					edit : true
				},
				
				averege_speed : {
					title : 'Averege_speed',
					width : '10%',
					edit : true
				},
				
				owner : {
					title : 'owner',
					width : '10%',
					edit : true
				},
				
				permetNo : {
					title : 'permetNo',
					width : '10%',
					edit : true
				},
				
				
				vechicle_type : {
					title : 'Vechicle_type',
					width : '10%',
					edit : true
				},
				other : {
					title : 'other',
					width : '10%',
					edit : true
				}
			}
		});
		$('#VechicleTableContainer').jtable('load');
	});
</script>

</head>
<body bgcolor="pink">
<div  >

		<h1 style="color: red" align="center">VechicleTableContainer</h1>
		<div id="VechicleTableContainer"></div>
	</div>
</body>
</html>