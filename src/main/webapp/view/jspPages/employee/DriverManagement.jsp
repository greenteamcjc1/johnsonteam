<!DOCTYPE>
<html>
<head>
<title>CRUD operations using jTable in J2EE</title>
<!-- Include one of jTable styles. -->
<!-- <link href="css/metro/blue/jtable.css" rel="stylesheet" type="text/css" />
<link href="css/jquery-ui-1.10.3.custom.css" rel="stylesheet" type="text/css" />
Include jTable script file.
<script src="js/jquery-1.8.2.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.10.3.custom.js" type="text/javascript"></script>
<script src="js/jquery.jtable.js" type="text/javascript"></script>
 -->
<script type="text/javascript">
	$(document).ready(function() {
		$('#DriverTableContainer').jtable({
			title : 'Driver Management',
			actions : {
				listAction : 'DController?action=create',
				updateAction : 'DController?action=update',
				deleteAction : 'DController?action=delete'
			},
			
			fields : {
				firstname :{ 
					title :'firstname',
					width :'10%',
					key :true,
					list :true,
					edit :false,
					create :true
				},
				
				lastname : {
					title : 'lastname',
					width : '10%',
					edit : true
				},
				
				adhar_number : {
					title : 'adhar_number',
					width : '10%',
					edit : true
				},
				
				licence_number : {
					title : 'licence_number',
					width : '10%',
					edit : true
				},
				
				address : {
					title : 'address',
					width : '10%',
					edit : true
				},
				
				mobile_Number : {
					title : 'mobile_Number',
					width : '10%',
					edit : true
				},
				
				
				vechicle_number : {
					title : 'vechicle_number',
					width : '10%',
					edit : true
				}, 
				
				dob : {
					title : 'dob',
					width : '10%',
					edit : true
				}
			}
		});
		$('#DriverTableContainer').jtable('load');
	});
</script>

</head>
<body bgcolor="black">
<div  >

		<h1 style="color:red" align="center">DriverTableContainer</h1>
		<div id="DriverTableContainer"></div>
	</div>
</body>
</html>