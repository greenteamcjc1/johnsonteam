<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>

        <!--End Slider Area-->
        <!--Start Sercice Area-->
        <section class="work_area section_padding">
            <div class="container">
               <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="page_title text-center">
                            <h2>Service we provide</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href="#"><img src="img/service_1.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-truck"></i>
                                <h4><a href="#">Moving locally or interstate</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href="#"><img src="img/service_2.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-fighter-jet"></i>
                                <h4><a href="#">Moving Overseas</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href="#"><img src="img/service_3.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-briefcase"></i>
                                <h4><a href="#">Corporate Relocation</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href="#"><img src="img/service_4.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-dropbox"></i>
                                <h4><a href="#">Commercial Relocation</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href="#"><img src="img/service_5.jpg" alt=""></a>
                            <div class="service_title">
                               <i class="fa fa-dropbox"></i>
                                <h4><a href="#">Parking</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href="#"><img src="img/service_6.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-shopping-cart"></i>
                                <h4><a href="#">Storage</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Service Area-->
        <!--Our Process Area -->
        <section class="process_area section_padding section_gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="page_title text-center">
                            <h2>Our Process</h2>
                        </div>
                    
                <div class="row">
                    <div class="process_area_menu">
                        
                                <a href=""><img src="img/process_1.png" alt=""></a>
                                <i class="process_title">book our service</i>
                                <a href=""><img src="img/process_2.png" alt=""></a>
                                <i class="process_title">we pack our things</i>
                                <a href=""><img src="img/process_3.png" alt=""></a>
                                <i class="process_title">we move you things</i>
                                <a href=""><img src="img/process_4.png" alt=""></a>
                                <i class="process_title">delevery you safely</i>
                           
                    </div></div></div>
                </div>
            </div>
        </section>
       
        <section class="price_plan_area section_padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    
                        <div class="page_title text-center">
                            <h2>our basic pricing plans</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                        <div class="single_price_list text-center">
                            <h3 class="catagory_price">basic</h3>
                            <p class="price_dolar"><span>&#36;</span>1500.00</p>
                            <div class="price_datails">
                                <p class="hidding_text"> Moving Locally Or Interstate </p>
                                <p>1 Vehicle</p>
                                <p>3 Helpers</p>
                                <p>Packing And Moving</p>
                                <p>Moved in 1 Day</p>
                                <p>24/7 Support</p>
                                <div class="submit_rice">
                                    <a href="" class="signup_button">sing up now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                        <div class="single_price_list text-center">
                            <h3 class="catagory_price">Standard</h3>
                            <p class="price_dolar"><span>&#36;</span>1800.00</p>
                            <div class="price_datails">
                                <p class="hidding_text">Commercial Relocation </p>
                                <p>3 Vehicle</p>
                                <p>5 Helpers</p>
                                <p>Packing And Moving</p>
                                <p>Moved in 1 Day</p>
                                <p>24/7 Support</p>
                                <div class="submit_rice">
                                    <a href="" class="signup_button">sing up now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                        <div class="single_price_list text-center">
                            <h3 class="catagory_price active_price">Unlimited</h3>
                            <p class="price_dolar"><span>&#36;</span>2500.00</p>
                            <div class="price_datails">
                                <p  class="hidding_text">Corporatate Relocation</p>
                                <p>10 Vehicle</p>
                                <p>10 Helpers</p>
                                <p>Packing And Moving</p>
                                <p>Moved in 10 Day</p>
                                <p>24/7 Support</p>
                                <div class="submit_rice">
                                    <a href="" class="signup_button active_button">sing up now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
                        <div class="single_price_list text-center">
                            <h3 class="catagory_price">premium</h3>
                            <p class="price_dolar"><span>&#36;</span>3500.00</p>
                            <div class="price_datails">
                                <p class="hidding_text">Moving Overseas</p>
                                <p>Vehicle Or Cargo</p>
                                <p>30 Helpers</p>
                                <p>Packing And Moving</p>
                                <p>Moved in 20 Day</p>
                                <p>24/7 Support</p>
                                <div class="submit_rice">
                                    <a href="" class="signup_button">sing up now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Our Price Table Area-->
        <!--Client Discription Area-->
        <section class="client_discription_area section_padding section_gray">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="page_title text-center">
                            <h2>what out customer say about us</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="single_client background_whaite">
                            <div class="client_img">
                                <img src="img/client_discription_1.png" alt="">
                            </div>
                            <div class="client_discription">
                                <h5>rik nelson</h5>
                                <p class="client_address">sydney,australia</p>
                                <p>Thank you for being a friend. Travelled down the road and back again. Your heart is true you're a pal and a confidant. Movin' on up to the east side. We finally got a piece of the pie. </p>
                                <div class="rating_area">
                                    <p class="text-left">100% recomended</p>
                                    <div class="ratting text-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="single_client background_whaite">
                            <div class="client_img">
                                <img src="img/client_discription_2.png" alt="">
                            </div>
                            <div class="client_discription">
                                <h5>fish burn</h5>
                                <p class="client_address">sydney,australia</p>
                                <p>The weather started getting rough - the tiny ship was tossed. If not for the courage of the fearless crew the Minnow would be lost. the Minnow would be lost.</p>
                                <div class="rating_area">
                                    <p class="text-left">100% recomended</p>
                                    <div class="ratting text-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Client Discription Area-->
        <!-- Newsletter Area -->
        




</body>
</html>