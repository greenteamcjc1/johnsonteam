<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>


<%response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
response.setHeader("Pragma", "no-cache");
response.setDateHeader("Expires", 0); %>



<div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
   
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

   
    <div class="carousel-inner">

      <div class="item active">
        <img src="img/1.jpg" alt="Los Angeles" style="width:100%;">
        <div class="carousel-caption">
          <h3>By Road</h3>
          <p>All India Permite</p>
        </div>
      </div>

      <div class="item">
        <img src="img/home2.jpg" alt="Chicago" style="width:100%;">
        <div class="carousel-caption">
          <h3>By Shipe</h3>
          <p>We Are Responsive To Diliver Goods</p>
        </div>
      </div>
    
      <div class="item">
        <img src="img/home3.jpg" alt="New York" style="width:100%;">
        <div class="carousel-caption">
          <h3>Storge</h3>
          <p>Safely Store</p>
        </div>
      </div>
  
    </div>

    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>


</body>
</html>