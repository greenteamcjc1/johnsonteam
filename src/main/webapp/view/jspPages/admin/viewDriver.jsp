 <!DOCTYPE html>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="com.alighthub.tbs.model.DriverPojo1"%>
<%@page import="com.alighthub.tbs.model.RegisterPojo1"%>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
	<div id="wrapper">

		<div id="page-inner">


			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="card-title">
								<div class="title">
									All Employee
									
									<script type="text/javascript"> 

 
											function display(){
													
													document.update_driver.action="driverDisplay";
													document.update_driver.submit();
												}

											function add(){
	
													document.update_driver.action="addDriver.tiles";
													document.update_driver.submit();
												}
											
											
											 
											function remove(){
												
												document.update_drive.action="removeDriver";
												document.update_drive.submit();
											}
											function updateDriver(){
													
												document.update_drive.action="updateDriver";
												document.update_drive.submit();
												alert("Record Updatation Successfully!!!!!");
												
											}



									</script>
									
										
									<form name="update_driver">
									
									<%
																			String msg=(String)request.getAttribute("msg");
																		%>
									<label style="margin-left: 400px; color: #CC0000;" ><%
										if(msg!=null){
									%> <%=msg%> <%
 	}
 %></label>
										<button type="submit" class="btn btn-default" onclick="display()"
											style="margin-left: 300px">Display Driver</button>
										

										<button type="submit" class="btn btn-default" onclick="add()"
											style="margin-left: 5px">Add Driver</button>
										 
										
									</form>

								</div>
							</div>
						</div>
						<div class="panel-body" style="margin-left: 200px">
						
						 <%
												 	DriverPojo1 pojo=(DriverPojo1)request.getAttribute("driverRecord");
												 %>
						 
						 <form class="form-inline" method="post" name="update_drive" >
								<div class="form-group" style="margin-left: 200px;">
									<table>
										<tr>
											<td><label>First Name</label></td>
											<td><input type="text" name="fname" class="form-control"
												id="exampleInputName2" placeholder="First Name" value="<%=pojo.getFirstName()%>"></td>
										</tr>

										<tr>
											<td><label>Last Name</label></td>
											<td><input type="text" name="lname" class="form-control"
												id="exampleInputName2" placeholder="Last Name" value=<%=pojo.getLastName()%>></td>
										 
										<tr>
											<td><label>DOB</label></td>
											<td><input type="text" name="dob"
												class="form-control" id="exampleInputName2"
												placeholder="Mobile Number" value=<%=pojo.getDOB() %>></td>
										</tr>

										<tr>
											<td><label>Licence Number</label></td>
											<td><input type="text" name="lNumber"
												class="form-control" id="exampleInputName2"
												placeholder="Address" value=<%=pojo.getLicenceNumber() %>></td>
												
										</tr>
										
										<tr>
											<td><label>Address</label></td>
											<td><input type="text" name="address"
												class="form-control" id="exampleInputName2"
												placeholder="Address" value=<%=pojo.getAddress() %>></td>
												
										</tr>
										
										<tr>
											<td><label>Mobile Number</label></td>
											<td><input type="text" name="mobile"
												class="form-control" id="exampleInputName2"
												placeholder="Address" value=<%=pojo.getMobileNumber() %>></td>
												
										</tr>
										
										<tr>
											<td><label>Vehicle Number</label></td>
											<td><%  
													Connection con=(Connection)session.getAttribute("DBConnection");
									
													String sql="select * from vehicle";
												try{	
													
													Statement statement=con.createStatement();
													ResultSet rs=statement.executeQuery(sql); 
													%> 
													
													 <select name="vNumber" 
												class="form-control" id="exampleInputName2" 
												>   
												
												<option>Select</option>
													 <% 
														
														while(rs.next()){ %> 
	
														<option> <%=rs.getString(1) %></option>
														
														 <% }%>
													 
													<%}catch(Exception e){ }%>
												 
							
												 
												    </select> </td>
										</tr>
	
										
										<tr>
												<td><input type="hidden" name="adhar_number" value=<%=pojo.getAdharNumber() %>></td>
										</tr>

										 	
											  	
										<tr>
												<td> <button type="submit" class="btn btn-default" style="margin-top: 20px;  width: 100px;" onclick="updateDriver()">Submit</button></td>
												<td> <button type="reset" class="btn btn-default" style="margin-top: 20px; margin-left: 20px; width: 100px; ">Reset</button></td>
										</tr>
										
										<tr>
												
										</tr>
									</table>

								</div>

							</form>
						 
						 
						</div>

					</div>
				</div>

			</div>

		</div>
		<!--  /. PAGE INNER  -->
	</div>
	<!-- /. WRAPPER -->

</body>
</html>