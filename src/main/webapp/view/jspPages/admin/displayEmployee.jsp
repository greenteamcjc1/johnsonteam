 <!DOCTYPE html>
<%@page import="com.alighthub.tbs.model.RegisterPojo1"%>
<%@page import="java.util.List"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>
	<div id="wrapper">

		<div id="page-inner">


			<div class="row">
				<div class="col-xs-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="card-title">
								<div class="title">
									All Employee
									
									<script type="text/javascript"> 

 
											function display(){
	
													document.employee.action="EmployeeDisplay";
													document.employee.submit();
												}

											function add(){
	
													document.employee.action="addEmployee.tiles";
													document.employee.submit();
												}
											
											
											function view(){
												
												document.emp.action="viewEmployee";
												document.emp.submit();
											}
											
											function remove(){
												alert("DO You Want Delete Employee");
												document.emp.action="removeEmploye";
												document.emp.submit();
												
											}



									</script>
									

									<form name="employee">
										<%
											String msg=(String)request.getAttribute("msg");
										%>
										
										
											<label style="margin-left: 400px; color: #CC0000;"  ><%
																																if(msg!=null){
																															%><%=msg%><%
																																}
																															%></label>
										
										<button type="submit" class="btn btn-default" onclick="display()"
											style="margin-left: 300px">Display Employee</button>
										

										<button type="submit" class="btn btn-default" onclick="add()"
											style="margin-left: 5px">Add Employee</button>
										 
										
									</form>

								</div>
							</div>
						</div>
						<div class="panel-body" style="margin-left: 200px">
						
						
					 <form name="emp">
					 <%
					 	List<RegisterPojo1> list=(List)request.getAttribute("employeeData");
					 %>
						
						<table  class="table">
						
						<tr>
						
				            <th> Select</th>
							
							<th> First Name</th>
							<th> Last Name </th>
							<th> Mobile Number</th>
							<th> Address</th>
							<th> Email </th>
						
						</tr>
						
						<%
													for(RegisterPojo1 pojo:list)
																{
												%>
							
							<tr class="success">
									 <td> <input type="radio" name="selectRecord" value="<%=pojo.getEmail() %>"></td> 
									 <td><% out.print(pojo.getFirstName()); %></td> 
									<td><% out.print(pojo.getLastName()); %></td>
									<td><% out.print(pojo.getMobileNo()); %></td>
									<td><% out.print(pojo.getAddress()); %></td>
									<td><% out.print(pojo.getEmail()); %></td>
								<td>	<button type="submit" class="btn btn-default" onclick="view()"
											style="margin-left: 5px;" >Update</button> </td>
						   		<td><button type="submit" class="btn btn-default" onclick="remove()">Remove</button></td>
								
							</tr>
							
							 
						<%} %>
						   
						   <tr>
						   			
						   </tr>
						
						</table>
					</form>
						</div>

					</div>
				</div>

			</div>

		</div>
		<!--  /. PAGE INNER  -->
	</div>
	<!-- /. WRAPPER -->

</body>
</html>