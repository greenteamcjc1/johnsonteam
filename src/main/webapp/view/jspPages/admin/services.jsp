<!DOCTYPE html>

    <head>
        
    </head>
    <body>
 
        <!--Start Sercice Area -->
        <section class="work_area section_padding section_gray">
            <div class="container">
               <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="page_title text-center">
                            <h2>Service we provide</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href=""><img src="img/service_1.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-truck"></i>
                                <h4><a href="#">Moving locally or interstate</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href=""><img src="img/service_2.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-fighter-jet"></i>
                                <h4><a href="#">Moving Overseas</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href=""><img src="img/service_3.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-briefcase"></i>
                                <h4><a href="#">Corporate Relocation</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href=""><img src="img/service_4.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-dropbox"></i>
                                <h4><a href="#">Commercial Relocation</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href=""><img src="img/service_5.jpg" alt=""></a>
                            <div class="service_title">
                               <i class="fa fa-dropbox"></i>
                                <h4><a href="#">Parking</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-6">
                        <div class="single_service">
                            <a href=""><img src="img/service_6.jpg" alt=""></a>
                            <div class="service_title">
                                <i class="fa fa-shopping-cart"></i>
                                <h4><a href="#">Storage</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Service Area-->
		
		
		 <!--Our Process Area -->
        <section class="process_area section_padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="page_title text-center">
                            <h2>Our Process</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="process_area_menu">
                        <ul>
                            <li>
                                <a href="#"><img src="img/process_1.png" alt=""></a>
                                <p class="process_title">book our service</p>
                            </li>
                            <li>
                                <a href="#"><img src="img/process_2.png" alt=""></a>
                                <p class="process_title">we pack our things</p>
                            </li>
                            <li>
                                <a href="#"><img src="img/process_3.png" alt=""></a>
                                <p class="process_title">we move you things</p>
                            </li>
                            <li>
                                <a href="#"><img src="img/process_4.png" alt=""></a>
                                <p class="process_title">delevery you safely</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--Our Process Area-->
       
    </body>
</html>